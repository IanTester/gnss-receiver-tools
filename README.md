# GNSS receiver tools

GNSS-receiver-tools is planned to be a set of tools for working with
GPS/GLONASS/Beidou/Galileo receivers. At the moment it works with
[NavSpark](http://www.navspark.com.tw/)s and likely any SkyTraq Venus 6 or 8
based receiver.

So far it mostly consists of lots of C++ classes representing the Venus 6/8
binary message protocol, as well as sending/receiving these messages.

A single `parse-skytraq` program is all that exists so far. It displays data
about some of the messages sent from the receiver. It also allows you to set
the update rate (1, 2, 4, 5, or 10 Hz) with '`-r`', and select between
text ('`-t`') or binary ('`-b`') output.

## Class documentation

The Doxygen-generated class documentation is available at [IanTester.codeberg.page/GNSS-receiver-tools](http://IanTester.codeberg.page/GNSS-receiver-tools)
