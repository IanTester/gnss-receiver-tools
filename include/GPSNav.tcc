/*
        Copyright 2021 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace GPS {

  template <typename T>
  bool Subframe::isa(void) const { return typeid(*this) == typeid(T); }

  template <typename T>
  T* Subframe::cast_as(void) {
    T *a = dynamic_cast<T*>(this);
    if (a == nullptr)
      throw std::bad_cast();
    return a;
  }

}; // namespace GPS
