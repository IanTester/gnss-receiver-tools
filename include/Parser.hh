/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <functional>
#include <deque>
#include <istream>
#include <map>
#include <queue>
#include "NMEA-0183.hh"
#include "SkyTraqBin.hh"
#include "UBX.hh"

namespace GNSS {

  class EndOfFile : public std::exception {
  private:

  public:
    EndOfFile() {}

    const char* what() const throw() {
      return "End of file";
    }
  }; // class EndOfFile


  class NotSendable : public std::exception {
  private:

  public:
    NotSendable() {}

    const char* what() const throw() {
      return "Is not sendable";
    }
  }; // class NotSendable


  //! Unified parser class
  class Parser {
  private:
    std::deque<uint8_t> _parse_buffer;

  public:
    //! Empty constructor
    Parser();

    //! Clear the parse buffer
    void reset_buffer(void);

    //! Add bytes to the end of the parse buffer
    void add_bytes(const std::vector<uint8_t>& buffer);

    //! Parse contents of the parse buffer into messages
    std::vector<Message::ptr> parse_messages(void);

  }; // class Parser


  class Interface;

#define EMPTY_HANDLER(name, type, argname) inline virtual void name(Interface* iface, const type &argname) {}

  //! Base class that receives parsed messages
  class Listener {
  public:
    inline Listener() {}

    inline virtual ~Listener() {}

    // handler methods for NMEA-0183 sentences
    EMPTY_HANDLER(GGA, NMEA0183::GGA, gga)
    EMPTY_HANDLER(GLL, NMEA0183::GLL, gll)
    EMPTY_HANDLER(GSA, NMEA0183::GSA, gsa)
    EMPTY_HANDLER(GSV, NMEA0183::GSV, gsv)
    EMPTY_HANDLER(RMC, NMEA0183::RMC, rmc)
    EMPTY_HANDLER(VTG, NMEA0183::VTG, vtg)
    EMPTY_HANDLER(ZDA, NMEA0183::ZDA, zda)

    using ptr = std::shared_ptr<Listener>;

  }; // class Listener


  //! Base class that receives parsed messages, including SkyTraq-specific NMEA and binary messages
  class Listener_SkyTraq : public Listener {
  public:
    // handler methods for proprietary SkyTraq sentences
    EMPTY_HANDLER(Skytraq_PPS, NMEA0183::Skytraq::PPS, pps)
    EMPTY_HANDLER(Skytraq_Sensors, NMEA0183::Skytraq::Sensors, sensors)

    // handler methods for periodic SkyTraq binary messages
    EMPTY_HANDLER(Navigation_data, SkyTraqBin::Nav_data_msg, nav)
    EMPTY_HANDLER(Sensor_data, SkyTraqBin::Sensor_data, sd)

    // handler methods for periodic NS-RAW binary messages
    EMPTY_HANDLER(Measurement_time, SkyTraqBin::Measurement_time, mt)
    EMPTY_HANDLER(Raw_measurements, SkyTraqBin::Raw_measurements, rm)
    EMPTY_HANDLER(SV_channel_status, SkyTraqBin::SV_channel_status, sv_chan)
    EMPTY_HANDLER(Rcv_state, SkyTraqBin::Rcv_state, rcv_state)
    EMPTY_HANDLER(GPS_subframe_data, SkyTraqBin::GPS_subframe_data, sfd)

  }; // class Listener_SkyTraq


  //! Base class that receives parsed messages, including Ublox-specific NMEA and binary messages
  class Listener_Ublox : public Listener {
  public:
    EMPTY_HANDLER(CFG_PRT, UBX::Cfg::Prt, prt)
    EMPTY_HANDLER(CFG_MSG, UBX::Cfg::Msg, msg)
    EMPTY_HANDLER(CFG_GNSS, UBX::Cfg::GNSS, gnss)
  }; // class Listener_Ublox

#undef EMPTY_HANDLER

  //! Class for an object that reads from a stream and calls methods in a Listener object
  class Interface {
  public:
    //! Type for a function called when a query/get input message gets a response
    /*!
      \param ack Did the input message receive an ack (true), or a nack (false)?
      \param msg The output message that was received

      There are three ways this lambda will be called:
      -# (false, nullptr) - Nack was received
      -# (true, nullptr) - Ack was received
      -# (true, msg) - Response message received

      The third variant only happens with response-type messages.
     */
    using ResponseHandler_Skytraq = std::function<void(bool ack, SkyTraqBin::Output_message* msg)>;

    using ResponseHandler_Ublox = std::function<void(bool ack, UBX::Output_message* msg)>;

  private:
    std::iostream &_file;
    bool _is_chrdev;
    Listener::ptr _listener;
    GNSS::Parser _parser;
    std::queue<std::vector<uint8_t> > _output_queue;
    bool _response_pending;
    std::map<uint16_t, ResponseHandler_Skytraq> _response_handlers_skytraq;
    std::map<uint16_t, ResponseHandler_Ublox> _response_handlers_ublox;

    void _send_from_queue(void);

  public:
    //! Constructor
    /*!
      \param f Open file handle
      \param l Listener object that will receive messages
     */
    Interface(std::iostream&& f, Listener::ptr l);

    //! Read a small amount of data from the file, parse it, send messages to the listener object
    void read(void);

    void set_is_chrdev(bool c = true);

    //! Send a message to the Skytraq device
    void send(SkyTraqBin::Input_message::ptr msg);

    //! Send a message to the Skytraq device, call lambda when response is received
    void send(SkyTraqBin::Input_message::ptr msg, ResponseHandler_Skytraq rh);

    //! Send a message to the Ublox device
    void send(UBX::Input_message::ptr msg);

    //! Send a message to the Ublox device, call lambda when response is received
    void send(UBX::Input_message::ptr msg, ResponseHandler_Ublox rh);

  }; // class Interface


}; // namespace SkyTraq
