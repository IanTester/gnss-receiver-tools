/*
        Copyright 2021 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/

template <typename T>
T bitstream::extract(std::ptrdiff_t src_start, uint8_t src_len) const {
  static uint8_t T_bits = sizeof(T) * 8;
  T value = 0;

  if (src_start < 0)
    src_start += _num_bits;

  if (src_len > T_bits)
    src_len = T_bits;

  _copy(src_start, src_len,
	[&](std::size_t byte_i, uint8_t bit_i) {
	  return _get_bit(byte_i, bit_i);
	},
	[&](std::size_t byte_i, uint8_t bit_i, bool data) {
	  bit_i += byte_i * 8;
	  value |= static_cast<T>(data) << bit_i;
	});

  return value;
}

template <typename T>
void bitstream::insert(const T src, std::ptrdiff_t dest_start, uint8_t dest_len) {
  static uint8_t T_bits = sizeof(T) * 8;

  if (dest_start < 0)
    dest_start += _num_bits;

  if (dest_len > T_bits)
    dest_len = T_bits;

  expand(dest_start + dest_len - 1);

  _copy(0, dest_len,
	[&](std::size_t byte_i, uint8_t bit_i) {
	  uint8_t mask = 1 << bit_i;
	  return src & mask;
	},
	[&](std::size_t byte_i, uint8_t bit_i, bool data) {
	  _set_bit(byte_i, bit_i, data);
	},
	dest_start);
}
