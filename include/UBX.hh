/*
        Copyright 2016 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdio>
#include <memory>
#include <sstream>
#include <math.h>
#include "GNSS.hh"

/*
  Sources:
*/

/* All input message class names shall start with a verb
   Common words shortened:
   configure => cfg
   download => dl
   image => img
   message => msg
   navigation => nav
   position => pos
   query => q
   software => sw
   system => sys
   version => ver
*/

namespace UBX {

  //! Type for the binary message length, limited to 64 KiB
  using Length = uint16_t;

  const uint8_t SyncChar[2] = { 0xB5, 0x62 };
  const Length SyncChar_len = 2;
  const Length Length_len = 2;
  const Length ClassID_len = 2;
  const Length Checksum_len = 2;


  enum class GNSS_ID : uint8_t {
    GPS = 0,
      SBAS,
      Galileo,
      BeiDou,
      IMES,
      QZSS,
      GLONASS,
  }; // enum class GNSS_ID

  enum class Class_ID : uint8_t {
    NAV = 1,
      RXM = 2,
      INF = 4,
      ACK = 5,
      CFG = 6,
      UPD = 9,
      MON = 10,
      AID = 11,
      TIM = 13,
      MGA = 19,
      LOG = 33,
  }; // enum class Class_ID

  enum class PortID : uint8_t {
    DDC = 0,
      I2C = 0,
      UART = 1,
      USB = 3,
      SPI = 4,
  }; // enum class PortID


  //! Exception class for when checksums don't match
  class ChecksumMismatch : public std::exception {
  private:
    uint16_t _computed_cs, _stream_cs;

  public:
    ChecksumMismatch(uint8_t ccs, uint8_t scs) :
      _computed_cs(ccs), _stream_cs(scs)
    {}

    const char* what() const throw() {
      std::ostringstream oss;
      oss.width(2);
      oss << "Checksum mismatch in SkyTraq binary stream - computed 0x" << std::hex << _computed_cs << ", found 0x" << _stream_cs;
      return oss.str().c_str();
    }
  }; // class ChecksumMismatch


  //! Exception class for when we can't find the message ID in our list
  class UnknownMessageID : public std::exception {
  private:
    uint8_t _cls, _id;

  public:
    UnknownMessageID(uint8_t cls, uint8_t id) :
      _cls(cls), _id(id)
    {}

    const char* what() const throw() {
      std::ostringstream oss;
      oss.width(2);
      oss.fill('0');
      oss << "Unknown message class 0x" << std::hex << static_cast<int>(_cls) << " / id 0x" << std::hex << static_cast<int>(_id);
      return oss.str().c_str();
    }
  }; // class UnknownMessageID


  //! Base class for a binary message
  class Message : public GNSS::Message {
  protected:
    Class_ID _cls;
    uint8_t _id;

    //! Constructor
    /*!
      \param cls Class for this message
      \param id ID for this message
    */
    Message(Class_ID cls, uint8_t id) :
      _cls(cls), _id(id)
    {}

  public:
    //! Getter method for the message class
    inline const Class_ID cls(void) const { return _cls; }

    //! Getter method for the message ID
    inline const uint8_t id(void) const { return _id; }

  }; // class Message


  //! Base class for messages that come from the GPS receiver
  class Output_message : public Message {
  protected:
    //! Constructor from a message class and ID
    Output_message(Class_ID cls, uint8_t id) :
      Message(cls, id)
    {}

  public:

    using ptr = std::shared_ptr<Output_message>;

  }; // class Output_message


  //! Parser
  Output_message::ptr parse_message(const std::vector<uint8_t>& payload);


  //! Base class for messages that go to the GPS receiver
  class Input_message : public Message {
  protected:
    //! The length of the payload (not including message class/ID)
    virtual inline const Length payload_length(void) const { return 0; }

    //! Write body fields into a pre-allocated buffer
    virtual inline void body_to_buf(std::vector<uint8_t>& payload) const { }

    //! Constructor from a message class and ID
    Input_message(Class_ID cls, uint8_t id) :
      Message(cls, id)
    {}

  public:
    //! The total length of the message
    inline const Length message_length(void) const { return SyncChar_len + ClassID_len + Length_len + payload_length() + Checksum_len; }

    //! Write the message into a buffer
    /*!
      \param buffer Pointer to a buffer, allocated and freed by the caller. Use
      message_length() to know how big the buffer needs to be.
     */
    virtual void to_buf(std::vector<uint8_t>& buffer) const;

    using ptr = std::shared_ptr<Input_message>;

  }; // class Input_message


  //! Base class for messages than be both sent and received
  class Input_Output_message : public Input_message, public Output_message {
  protected:
    //! Constructor from a message class and ID
    Input_Output_message(Class_ID cls, uint8_t id) :
      Input_message(cls, id),
      Output_message(cls, id)
    {}

  public:
  };


#define GETTER(type, name, field) inline const type name(void) const { return field; }

#define GETTER_SETTER(type, name, field) inline const type name(void) const { return field; } \
inline void set_##name(type val) { field = val; }

#define SETTER_BOOL(name, field) inline void set_##name(bool val=true) { field = val; } \
inline void unset_##name(void) { field = false; }

#define GETTER_RAW(type, name, field) inline const type name##_raw(void) const { return field; }

#define GETTER_SETTER_RAW(type, name, field) inline const type name##_raw(void) const { return field; } \
inline void set_##name##_raw(type val) { field = val; }

#define GETTER_MOD(type, name, code) inline const type name(void) const { return code; }

#define GETTER_SETTER_MOD(type, name, field, code_get, code_set) inline const type name(void) const { return code_get; } \
inline void set_##name(type val) { field = code_set; }


}; // namepace UBX

#include "UBX_ack.hh"
#include "UBX_cfg.hh"

#define ENUM_OSTREAM_OPERATOR(type) inline std::ostream& operator<< (std::ostream& out, type val) { out << std::to_string(val); return out;  }

namespace std {
  std::string to_string(UBX::GNSS_ID id);
  ENUM_OSTREAM_OPERATOR(UBX::GNSS_ID);

  std::string to_string(UBX::PortID id);
  ENUM_OSTREAM_OPERATOR(UBX::PortID);

  std::string to_string(UBX::Cfg::txReady_polarity pol);
  ENUM_OSTREAM_OPERATOR(UBX::Cfg::txReady_polarity);

  std::string to_string(UBX::Cfg::Mode_Charlen charlen);
  ENUM_OSTREAM_OPERATOR(UBX::Cfg::Mode_Charlen);

  std::string to_string(UBX::Cfg::Mode_Parity par);
  ENUM_OSTREAM_OPERATOR(UBX::Cfg::Mode_Parity);

  std::string to_string(UBX::Cfg::Mode_Stopbits bits);
  ENUM_OSTREAM_OPERATOR(UBX::Cfg::Mode_Stopbits);

}; // namespace std

// Undefine our macros here, unless Doxygen is reading this
#ifndef DOXYGEN_SKIP_FOR_USERS
#undef ENUM__OSTREAM_OPERATOR
#undef GETTER
#undef GETTER_SETTER
#undef SETTER_BOOL
#undef GETTER_RAW
#undef GETTER_SETTER_RAW
#undef GETTER_MOD
#undef GETTER_SETTER_MOD

#endif
