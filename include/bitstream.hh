/*
        Copyright 2021 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <functional>
#include <iostream>
#include <vector>
#include <stdint.h>

//! Class for holding a stream of bits, putting data in and extracting it
class bitstream {
protected:
  std::vector<uint8_t> _bytes;
  std::size_t _num_bits;

  bool _get_bit(std::size_t byte_i, uint8_t bit_i) const;
  void _set_bit(std::size_t byte_i, uint8_t bit_i, bool data);

  static void _copy(const std::size_t src_start, const std::size_t len,
		    std::function<bool(std::size_t byte_i, uint8_t bit_i)> src_get,
		    std::function<void(std::size_t byte_i, uint8_t bit_i, bool data)> dest_put,
		    std::size_t dest_start = 0);

  friend std::ostream& operator<<(std::ostream& out, const bitstream& bs);

public:
  //! Empty constructor
  bitstream();

  //! Constructor that sets the size and fills the buffer
  bitstream(std::size_t count, bool val = false);

  //! Size of the bitstream, in bits
  std::size_t size() const;

  //! Expand storage to a number of bits
  void expand(std::size_t size);

  //! Clear the byte array
  void clear(void);

  //! Copy bits from one bitstream to another
  /*!
    self[dest_start:dest_start+len] = src[src_start:src_start+len]

    \param src		Source bitstream
    \param src_start	Bit index to start copying from src
    \param len		How many bits to copy
    \param dest_start	Bit index to start copying to dest
  */
  void copy(const bitstream& src, std::ptrdiff_t src_start,
	    const std::size_t len,
	    std::ptrdiff_t dest_start = 0);

  //! Extract a piece of data from a bitstream
  /*!
    \param src_start	Bit index to start extracting from src
    \param src_len	Number of bits to extract, defaults to size of T
    \return		self[src_start:src_start + src_len]
  */
  template <typename T>
  T extract(std::ptrdiff_t src_start, uint8_t src_len = sizeof(T) * 8) const;

  //! Insert a piece of data into a bitstream
  /*!
    self[dest_start:dest_start + sizeof(T)] = src

    \param src		Piece of data
    \param dest_start	Bit index to start copying to dest
    \param dest_len	Number of bits to inset, dedaults to size of T
  */
  template <typename T>
  void insert(const T src, std::ptrdiff_t dest_start, uint8_t dest_len = sizeof(T) * 8);

}; // class bitstream

std::ostream& operator<<(std::ostream& out, const bitstream& bs);

#include "bitstream.tcc"
