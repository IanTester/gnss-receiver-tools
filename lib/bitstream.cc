/*
        Copyright 2021 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "bitstream.hh"
#include <algorithm>

bitstream::bitstream() :
  _num_bits(0)
{}

bitstream::bitstream(std::size_t count, bool val) :
  _bytes((count >> 3) + (count & 0x03 ? 1 : 0)),
  _num_bits(count)
{
  for (std::size_t i = 0; i < _bytes.size(); i++)
    _bytes[i] = val ? 0xff : 0x00;

  for (std::size_t i = _bytes.size() << 3; i < count; i++)
    _set_bit(i >> 3, i & 0x07, val);
}

std::size_t bitstream::size() const {
  return _num_bits;
}

void bitstream::expand(std::size_t size) {
  std::size_t bytes = (size >> 3) + (size & 0x03 ? 1 : 0);
  _bytes.reserve(bytes);

  while (_bytes.size() < bytes)
    _bytes.push_back(0);

  _num_bits = size;
}

void bitstream::clear(void) {
  _bytes.clear();
}

void bitstream::_copy(const std::size_t src_start, const std::size_t len,
		      std::function<bool(std::size_t byte_i, uint8_t bit_i)> src_get,
		      std::function<void(std::size_t byte_i, uint8_t bit_i, bool data)> dest_put,
		      std::size_t dest_start) {

  // Super simple implementation
  for (std::size_t i = 0; i < len; i++) {
    auto src_byte = (src_start + i) >> 3;
    uint8_t src_bit = (src_start + i) & 0x07;

    auto dest_byte = (dest_start + i) >> 3;
    uint8_t dest_bit = (dest_start + i) & 0x07;

    bool bit = src_get(src_byte, src_bit);
    dest_put(dest_byte, dest_bit, bit);
  }
}

bool bitstream::_get_bit(std::size_t byte_i, uint8_t bit_i) const {
  uint8_t mask = 1 << bit_i;
  return _bytes.at(byte_i) & mask;
}

void bitstream::_set_bit(std::size_t byte_i, uint8_t bit_i, bool data) {
  uint8_t mask = 1 << bit_i;
  _bytes.at(byte_i) = (_bytes.at(byte_i) & ~mask) | (static_cast<uint8_t>(data) << bit_i);
}

void bitstream::copy(const bitstream& src, std::ptrdiff_t src_start,
		     const std::size_t len,
		     std::ptrdiff_t dest_start) {

  if (src_start < 0)
    src_start += src._num_bits;

  if (dest_start < 0)
    dest_start += _num_bits;

  expand(dest_start + len - 1);

  _copy(src_start, len,
	[&](std::size_t byte_i, uint8_t bit_i) {
	  return src._get_bit(byte_i, bit_i);
	},
	[&](std::size_t byte_i, uint8_t bit_i, bool data) {
	  _set_bit(byte_i, bit_i, data);
	},
	dest_start);
}

std::ostream& operator<<(std::ostream& out, const bitstream& bs) {
  uint8_t to_skip = (bs._bytes.size() * 8) - bs._num_bits;

  std::for_each(bs._bytes.rbegin(), bs._bytes.rend(),
		[&](const uint8_t byte) {
		  uint8_t mask = 0x80;
		  while (mask) {
		    if (to_skip)
		      to_skip--;
		    else
		      out << (byte & mask ? "1" : "0");

		    mask >>= 1;
		  }
		});
  out << "b";

  return out;
}
