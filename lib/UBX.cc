/*
        Copyright 2016 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <functional>
#include <iostream>
#include <iomanip>
#include <map>
#include <stdexcept>
#include <endian.h>
#include <stdlib.h>
#include <string.h>
#include "UBX.hh"
#include "LE.hh"

/*
  Sources:
*/

namespace UBX {

  struct checksum_t {
    uint8_t A, B;
    operator uint16_t() const { return A | (static_cast<uint16_t>(B) << 8); }
  };

  checksum_t checksum(const std::vector<uint8_t>::const_iterator buffer, const std::vector<uint8_t>::const_iterator buffer_end) {
    checksum_t ck = { 0, 0};

    for (auto i = buffer; i != buffer_end; i++) {
      ck.A += *i;
      ck.B += ck.A;
    }

    return ck;
  }

  void Input_message::to_buf(std::vector<uint8_t>& buffer) const {
    append_le(buffer, SyncChar[0]);
    append_le(buffer, SyncChar[1]);

    append_le(buffer, static_cast<uint8_t>(_cls));
    append_le(buffer, _id);

    Length payload_len = payload_length();
    append_le(buffer, payload_len);

    auto payload_start = buffer.size();
    body_to_buf(buffer);

    checksum_t cs = checksum(buffer.cbegin() + payload_start, buffer.cend());
    append_le(buffer, cs.A);
    append_le(buffer, cs.B);
  }


  using output_message_factory = std::function<Output_message::ptr(std::vector<uint8_t>::const_iterator&, const std::vector<uint8_t>::const_iterator&)>;
#define OUTPUT(CLS, ID, CLASS) std::make_pair<uint16_t, output_message_factory>((static_cast<uint8_t>(CLS) << 8) | (ID), [](std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) { return std::make_shared<CLASS>(payload, payload_last); })
  std::map<uint16_t, output_message_factory> output_message_factories = {
    OUTPUT(Class_ID::ACK, 0x00, Ack::Nak),
    OUTPUT(Class_ID::ACK, 0x01, Ack::Ack),

    OUTPUT(Class_ID::CFG, 0x00, Cfg::Prt),
    OUTPUT(Class_ID::CFG, 0x01, Cfg::Msg),
    OUTPUT(Class_ID::CFG, 0x3e, Cfg::GNSS),
  };

#undef OUTPUT

  Output_message::ptr parse_message(const std::vector<uint8_t>& buffer) {
    auto buffer_start = buffer.cbegin() + SyncChar_len + ClassID_len;
    Length payload_len = extract_le<uint16_t>(buffer_start);
    std::size_t total_length = SyncChar_len + ClassID_len + Length_len + payload_len + Checksum_len;
    if (buffer.size() < total_length)
      throw GNSS::InsufficientData();

    if ((buffer[0] != SyncChar[0])
	|| (buffer[1] != SyncChar[1]))
      throw GNSS::InvalidMessage();

    checksum_t cs = { buffer[total_length - 1], buffer[total_length] };
    {
      checksum_t ccs = checksum(buffer.cbegin() + SyncChar_len, buffer.cbegin() + SyncChar_len + ClassID_len + payload_len);
      if ((cs.A != ccs.A) && (cs.B != ccs.B))
	throw ChecksumMismatch(ccs, cs);
    }

    uint8_t cls = buffer[SyncChar_len], id = buffer[SyncChar_len + 1];
    uint16_t clsid = (static_cast<uint16_t>(cls) << 8) | id;
    if (output_message_factories.count(clsid) == 0)
      throw UnknownMessageID(cls, id);

    buffer_start = buffer.cbegin();
    auto buffer_last = buffer.cbegin() + SyncChar_len + ClassID_len + Length_len - 1;
    return output_message_factories[clsid](buffer_start, buffer_last);
  }

}; // namespace UBX

namespace std {

  string to_string(UBX::GNSS_ID id) {
    switch (id) {
    case UBX::GNSS_ID::GPS:
      return "GPS";
    case UBX::GNSS_ID::SBAS:
      return "SBAS";
    case UBX::GNSS_ID::Galileo:
      return "Galileo";
    case UBX::GNSS_ID::BeiDou:
      return "BeiDou";
    case UBX::GNSS_ID::IMES:
      return "IMES";
    case UBX::GNSS_ID::QZSS:
      return "QZSS";
    case UBX::GNSS_ID::GLONASS:
      return "GLONASS";
    }
    throw invalid_argument("Unrecognised value for GNSS_ID");
  }

  string to_string(UBX::PortID id) {
    switch (id) {
    case UBX::PortID::DDC:
      return "DDC";
    case UBX::PortID::UART:
      return "UART";
    case UBX::PortID::USB:
      return "USB";
    case UBX::PortID::SPI:
      return "SPI";
    }
    throw invalid_argument("Unrecognised value for PortID");
  }

  string to_string(UBX::Cfg::txReady_polarity pol) {
    switch (pol) {
    case UBX::Cfg::txReady_polarity::Hi_active:
      return "hi active";
    case UBX::Cfg::txReady_polarity::Lo_active:
      return "lo active";
    }
    throw invalid_argument("Unrecognised value for txReady_polarity");
  }

  string to_string(UBX::Cfg::Mode_Charlen charlen) {
    switch (charlen) {
    case UBX::Cfg::Mode_Charlen::Bits_5:
      return "5";
    case UBX::Cfg::Mode_Charlen::Bits_6:
      return "6";
    case UBX::Cfg::Mode_Charlen::Bits_7:
      return "7";
    case UBX::Cfg::Mode_Charlen::Bits_8:
      return "8";
    }
    throw invalid_argument("Unrecognised value for Mode_Charlen");
  }

  string to_string(UBX::Cfg::Mode_Parity par) {
    switch (par) {
    case UBX::Cfg::Mode_Parity::Even:
      return "even";
    case UBX::Cfg::Mode_Parity::Odd:
      return "odd";
    case UBX::Cfg::Mode_Parity::Reserved:
      return "reserved";
    case UBX::Cfg::Mode_Parity::None:
      return "none";
    }
    throw invalid_argument("Unrecognised value for Mode_Parity");
  }

  string to_string(UBX::Cfg::Mode_Stopbits bits) {
    switch (bits) {
    case UBX::Cfg::Mode_Stopbits::Bits_1:
      return "1";
    case UBX::Cfg::Mode_Stopbits::Bits_1_5:
      return "1.5";
    case UBX::Cfg::Mode_Stopbits::Bits_2:
      return "2";
    case UBX::Cfg::Mode_Stopbits::Bits_0_5:
      return "0.5";
    }
    throw invalid_argument("Unrecognised value for Mode_Stopbits");
  }

}; // namespace std
