/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <functional>
#include <iostream>
#include <iomanip>
#include <map>
#include <stdexcept>
#include <endian.h>
#include <stdlib.h>
#include <string.h>
#include "SkyTraqBin.hh"
#include "BE.hh"

/*
  Sources:
  https://store-lgdi92x.mybigcommerce.com/content/AN0028_1.4.31.pdf	(Binary messages of Skytraq Venus 8)
  https://store-lgdi92x.mybigcommerce.com/content/AN0024_v07.pdf	(Raw measurement binary messages of Skytraq 6 & 8)
  https://store-lgdi92x.mybigcommerce.com/content/SUP800F_v0.6.pdf	(Skytraq SUP800F datasheet)
  https://store-lgdi92x.mybigcommerce.com/content/AN0008_v1.4.17.pdf    (Datalogging extension for Venus 8)
*/

namespace SkyTraqBin {

  ChecksumMismatch::ChecksumMismatch(uint8_t ccs, uint8_t scs) :
    _computed_cs(ccs), _stream_cs(scs)
  {}

  const char* ChecksumMismatch::what() const throw() {
    std::ostringstream oss;
    oss.width(2);
    oss << "Checksum mismatch in SkyTraq binary stream - computed 0x" << std::hex << static_cast<int>(_computed_cs) << ", found 0x" << static_cast<int>(_stream_cs);
    return oss.str().c_str();
  }


  UnknownMessageID::UnknownMessageID(uint8_t i) :
    _id(i)
  {}

  const char* UnknownMessageID::what() const throw() {
    std::ostringstream oss;
    oss.width(2);
    oss.fill('0');
    oss << "Unknown message id 0x" << std::hex << static_cast<int>(_id);
    return oss.str().c_str();
  }


  Message::Message(uint8_t id) :
    _msg_id(id)
  {}

  const uint8_t Message::message_id(void) const {
    return _msg_id;
  }


  Output_message::Output_message(std::vector<uint8_t>::const_iterator& payload) :
    Message(extract_be<uint8_t>(payload))
  {}




  uint8_t checksum(const std::vector<uint8_t>::const_iterator buffer, const std::vector<uint8_t>::const_iterator buffer_end) {
    uint8_t cs = 0;
    for (auto i = buffer; i != buffer_end; i++)
      cs ^= *i;

    return cs;
  }


  using output_message_factory = std::function<Output_message::ptr(std::vector<uint8_t>::const_iterator&, const std::vector<uint8_t>::const_iterator&)>;
#define OUTPUT1(ID, CLASS) std::make_pair<uint16_t, output_message_factory>((ID), [](std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) { return std::make_shared<CLASS>(payload, payload_last); })
#define OUTPUT2(ID, SUBID, CLASS) std::make_pair<uint16_t, output_message_factory>(((ID) << 8) | (SUBID), [](std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) { return std::make_shared<CLASS>(payload, payload_last); })

  std::map<uint16_t, output_message_factory> output_message_factories = {
    OUTPUT2(0x62, 0x80, GNSS_SBAS_status),
    OUTPUT2(0x62, 0x81, GNSS_QZSS_status),
    OUTPUT2(0x63, 0x80, GNSS_SAEE_status),
    OUTPUT2(0x64, 0x80, GNSS_boot_status),
    OUTPUT2(0x64, 0x81, GNSS_extended_NMEA_msg_interval),
    OUTPUT2(0x64, 0x83, GNSS_interference_detection_status),
    OUTPUT2(0x64, 0x65, GPS_param_search_engine_num),
    OUTPUT2(0x64, 0x8B, GNSS_nav_mode),
    OUTPUT2(0x64, 0x8C, GNSS_constellation_type),
    OUTPUT2(0x64, 0x8E, GNSS_time),
    OUTPUT2(0x65, 0x80, GNSS_1PPS_pulse_width),
    OUTPUT2(0x64, 0x81, GNSS_1PPS_freq_output),
    OUTPUT2(0x64, 0x92, GNSS_datum_index),
    OUTPUT1(0x80, Sw_ver),
    OUTPUT1(0x81, Sw_CRC),
    OUTPUT1(0x83, Ack),
    OUTPUT1(0x84, Nack),
    OUTPUT1(0x86, Pos_update_rate),
    OUTPUT1(0x87, GPS_almanac_data),
    OUTPUT1(0x89, Bin_measurement_data_output_status),
    OUTPUT1(0x90, Glonass_ephemeris_data),
    OUTPUT1(0x93, NMEA_talker_ID),
    OUTPUT1(0x94, Log_status_output),
    OUTPUT1(0xA8, Nav_data_msg),
    OUTPUT1(0xAE, GNSS_datum),
    OUTPUT1(0xAF, GNSS_DOP_mask),
    OUTPUT1(0xB0, GNSS_elevation_CNR_mask),
    OUTPUT1(0xB1, GPS_ephemeris_data),
    OUTPUT1(0xB4, GNSS_pos_pinning_status),
    OUTPUT1(0xB9, GNSS_power_mode_status),
    OUTPUT1(0xBB, GNSS_1PPS_cable_delay),
    OUTPUT2(0xCF, 0x01, Sensor_data),
    OUTPUT1(0xDC, Measurement_time),
    OUTPUT1(0xDD, Raw_measurements),
    OUTPUT1(0xDE, SV_channel_status),
    OUTPUT1(0xDF, Rcv_state),
    OUTPUT1(0xE0, GPS_subframe_data),
    OUTPUT1(0xE1, Glonass_string_data),
    OUTPUT1(0xE2, Beidou2_subframe_data),
    OUTPUT1(0xE3, Beidou2_subframe_data),
  };
#undef OUTPUT1
#undef OUTPUT2


  Output_message::ptr parse_message(const std::vector<uint8_t>& buffer) {
    auto buffer_start = buffer.cbegin();
    Payload_length payload_len = extract_be<uint16_t>(buffer_start);
    std::size_t total_length = StartSeq_len + PayloadLength_len + payload_len + Checksum_len + EndSeq_len;
    if (buffer.size() < total_length)
      throw GNSS::InsufficientData();

    if ((buffer[0] != 0xa0)
	|| (buffer[1] != 0xa1)
	|| (buffer[total_length - 2] != 0x0d)
	|| (buffer[total_length - 1] != 0x0a))
      throw GNSS::InvalidMessage();

    std::size_t payload_start = StartSeq_len + PayloadLength_len;
    uint16_t id = buffer[payload_start];
    if ((id >= 0x62) && (id <= 0x65))	// construct a composite id if the message has a sub-ID
      id = (buffer[payload_start] << 8) | buffer[payload_start + 1];

    if (output_message_factories.count(id) == 0)
      throw UnknownMessageID(id);

    uint8_t cs = buffer[total_length - 3];
    {
      uint8_t ccs = checksum(buffer.cbegin() + payload_start, buffer.cend());
      if (cs != ccs)
	throw ChecksumMismatch(ccs, cs);
    }

    buffer_start = buffer.cbegin();
    auto buffer_last = buffer.cend() - 1;
    return output_message_factories[id](buffer_start, buffer_last);
  }


  const Payload_length Input_message::body_length(void) const {
    return 0;
  }

  void Input_message::body_to_buf(std::vector<uint8_t>& buffer) const
  {}

  Input_message::Input_message(uint8_t id) :
    Message(id)
  {}

  const Payload_length Input_message::message_length(void) const {
    return StartSeq_len + PayloadLength_len + MsgID_len + body_length() + Checksum_len + EndSeq_len;
  }

  void Input_message::to_buf(std::vector<uint8_t>& buffer) const {
    append_be<uint8_t>(buffer, 0xa0);
    append_be<uint8_t>(buffer, 0xa1);

    Payload_length payload_len = body_length() + MsgID_len;
    append_be(buffer, payload_len);

    auto payload_start = buffer.size();
    append_be(buffer, _msg_id);
    body_to_buf(buffer);

    append_be(buffer, checksum(buffer.cbegin() + payload_start, buffer.cend()));
    append_be<uint8_t>(buffer, 0x0d);
    append_be<uint8_t>(buffer, 0x0a);
  }


#define GETTER(type, class, name, field) const type class::name(void) const { return field; }


  with_subid::with_subid(uint8_t subid) :
    _msg_subid(subid)
  {}

  GETTER(uint8_t, with_subid, message_subid, _msg_subid);

  BaudRate rate_to_BaudRate(unsigned int rate) {
    if (rate > 921600)
      throw std::invalid_argument("Baud rate too high");

    if (rate > 460800)
      return BaudRate::Baud921600;

    if (rate > 230400)
      return BaudRate::Baud460800;

    if (rate > 115200)
      return BaudRate::Baud230400;

    if (rate > 57600)
      return BaudRate::Baud115200;

    if (rate > 38400)
      return BaudRate::Baud57600;

    if (rate > 19200)
      return BaudRate::Baud38400;

    if (rate > 9600)
      return BaudRate::Baud19200;

    if (rate > 4800)
      return BaudRate::Baud9600;

    return BaudRate::Baud4800;
  }

  unsigned int BaudRate_rate(BaudRate br) {
    switch (br) {
    case BaudRate::Baud4800:
      return 4800;
      break;
    case BaudRate::Baud9600:
      return 9600;
      break;
    case BaudRate::Baud19200:
      return 19200;
      break;
    case BaudRate::Baud38400:
      return 38400;
      break;
    case BaudRate::Baud57600:
      return 57600;
      break;
    case BaudRate::Baud115200:
      return 115200;
      break;
    case BaudRate::Baud230400:
      return 230400;
      break;
    case BaudRate::Baud460800:
      return 460800;
      break;
    case BaudRate::Baud921600:
      return 921600;
      break;
    }

    throw std::invalid_argument("Unrecognised baud rate");
  }

  OutputRate Hz_to_OutputRate(unsigned int hz) {
    if (hz < 2)
      return OutputRate::Rate1Hz;

    if (hz < 4)
      return OutputRate::Rate2Hz;

    if (hz < 5)
      return OutputRate::Rate4Hz;

    if (hz < 10)
      return OutputRate::Rate5Hz;

    if (hz < 20)
      return OutputRate::Rate10Hz;

    if (hz == 20)
      return OutputRate::Rate20Hz;

    throw std::invalid_argument("Output rate too high");
  }

  unsigned int OutputRate_Hz(OutputRate r) {
    switch (r) {
    case OutputRate::Rate1Hz:
      return 1;
      break;
    case OutputRate::Rate2Hz:
      return 2;
      break;
    case OutputRate::Rate4Hz:
      return 4;
      break;
    case OutputRate::Rate5Hz:
      return 5;
      break;
    case OutputRate::Rate10Hz:
      return 10;
      break;
    case OutputRate::Rate20Hz:
      return 20;
      break;
    }

    throw std::invalid_argument("Unrecognised output rate");
  }


}; // namespace SkyTraqBin

#undef GETTER

#define ENUM_OSTREAM_OPERATOR(type) std::ostream& operator<< (std::ostream& out, type val) { out << std::to_string(val); return out;  }

namespace std {

  string to_string(SkyTraqBin::StartMode mode) {
    switch (mode) {
    case SkyTraqBin::StartMode::HotStart:
      return "hot start";
    case SkyTraqBin::StartMode::WarmStart:
      return "warm start";
    case SkyTraqBin::StartMode::ColdStart:
      return "cold start";
    }
    throw invalid_argument("Unrecognised value for StartMode");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::StartMode);

  string to_string(SkyTraqBin::SwType sw) {
    switch (sw) {
    case SkyTraqBin::SwType::SystemCode:
      return "system code";
    }
    throw invalid_argument("Unrecognised value for SwType");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::SwType);

  string to_string(SkyTraqBin::BaudRate r) {
    return to_string(SkyTraqBin::BaudRate_rate(r));
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::BaudRate);

  string to_string(SkyTraqBin::UpdateType ut) {
    switch (ut) {
    case SkyTraqBin::UpdateType::SRAM:
      return "SRAM";
    case SkyTraqBin::UpdateType::SRAM_and_flash:
      return "SRAM and Flash";
    case SkyTraqBin::UpdateType::Temporary:
      return "temporary";
    }
    throw invalid_argument("Unrecognised value for UpdateType");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::UpdateType);

  string to_string(SkyTraqBin::MessageType mt) {
    switch (mt) {
    case SkyTraqBin::MessageType::None:
      return "none";
      break;
    case SkyTraqBin::MessageType::NMEA0183:
      return "NMEA-0183";
      break;
    case SkyTraqBin::MessageType::Binary:
      return "binary";
      break;
    }
    throw invalid_argument("Unrecognised value for MessageType");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::MessageType);

  string to_string(SkyTraqBin::FlashType ft) {
    switch (ft) {
    case SkyTraqBin::FlashType::Auto:
      return "auto";
    case SkyTraqBin::FlashType::QSPI_Winbond:
      return "QSPI Winbond";
    case SkyTraqBin::FlashType::QSPI_EON:
      return "QSPI Eon";
    case SkyTraqBin::FlashType::Parallel_Numonyx:
      return "parallel Flash Numonyx";
    case SkyTraqBin::FlashType::Parallel_EON:
      return "parallel Flash Eon";
    }
    throw invalid_argument("Unrecognised value for FlashType");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::FlashType);

  string to_string(SkyTraqBin::BufferUsed bu) {
    switch (bu) {
    case SkyTraqBin::BufferUsed::Size8K:
      return "8 KiB";
    case SkyTraqBin::BufferUsed::Size16K:
      return "16 KiB";
    case SkyTraqBin::BufferUsed::Size24K:
      return "24 KiB";
    case SkyTraqBin::BufferUsed::Size32K:
      return "32 KiB";
    }
    throw invalid_argument("Unrecognised value for BufferUsed");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::BufferUsed);

  string to_string(SkyTraqBin::PowerMode pm) {
    switch (pm) {
    case SkyTraqBin::PowerMode::Normal:
      return "normal";
    case SkyTraqBin::PowerMode::PowerSave:
      return "power save";
    }
    throw invalid_argument("Unrecognised value for PowerMode");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::PowerMode);

  string to_string(SkyTraqBin::OutputRate o) {
    switch (o) {
    case SkyTraqBin::OutputRate::Rate1Hz:
      return "1 Hz";
    case SkyTraqBin::OutputRate::Rate2Hz:
      return "2 Hz";
    case SkyTraqBin::OutputRate::Rate4Hz:
      return "4 Hz";
    case SkyTraqBin::OutputRate::Rate5Hz:
      return "5 Hz";
    case SkyTraqBin::OutputRate::Rate10Hz:
      return "10 Hz";
    case SkyTraqBin::OutputRate::Rate20Hz:
      return "20 Hz";
    }
    throw invalid_argument("Unrecognised value for OutputRate");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::OutputRate);

  string to_string(SkyTraqBin::DOPmode mode) {
    switch (mode) {
    case SkyTraqBin::DOPmode::Disable:
      return "disable";
    case SkyTraqBin::DOPmode::Auto:
      return "automatic";
    case SkyTraqBin::DOPmode::PDOP_only:
      return "PDOP only";
    case SkyTraqBin::DOPmode::HDOP_only:
      return "HDOP only";
    case SkyTraqBin::DOPmode::GDOP_only:
      return "GDOP only";
    }
    throw invalid_argument("Unrecognised value for DOPmode");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::DOPmode);

  string to_string(SkyTraqBin::ElevationCNRmode mode) {
    switch (mode) {
    case SkyTraqBin::ElevationCNRmode::Disable:
      return "disable";
    case SkyTraqBin::ElevationCNRmode::ElevationCNR:
      return "elevation and CNR";
    case SkyTraqBin::ElevationCNRmode::Elevation_only:
      return "elevation only";
    case SkyTraqBin::ElevationCNRmode::CNR_only:
      return "CNR only";
    }
    throw invalid_argument("Unrecognised value for ElevationCNRmode");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::ElevationCNRmode);

  string to_string(SkyTraqBin::DefaultOrEnable doe) {
    switch (doe) {
    case SkyTraqBin::DefaultOrEnable::Default:
      return "default";
    case SkyTraqBin::DefaultOrEnable::Enable:
      return "enable";
    case SkyTraqBin::DefaultOrEnable::Disable:
      return "disable";
    }
    throw invalid_argument("Unrecognised value for DefaultOrEnable");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::DefaultOrEnable);

  string to_string(SkyTraqBin::TalkerID id) {
    switch (id) {
    case SkyTraqBin::TalkerID::GP:
      return "GP mode";
    case SkyTraqBin::TalkerID::GN:
      return "GN mode";
    }
    throw invalid_argument("Unrecognised value for TalkerID");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::TalkerID);

  string to_string(SkyTraqBin::EnableOrAuto eoa) {
    switch (eoa) {
    case SkyTraqBin::EnableOrAuto::Disable:
      return "disable";
    case SkyTraqBin::EnableOrAuto::Enable:
      return "enable";
    case SkyTraqBin::EnableOrAuto::Auto:
      return "automatic";
    }
    throw invalid_argument("Unrecognised value for EnableOrAuto");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::EnableOrAuto);

  string to_string(SkyTraqBin::ParameterSearchEngineMode psem) {
    switch (psem) {
    case SkyTraqBin::ParameterSearchEngineMode::Default:
      return "default";
    case SkyTraqBin::ParameterSearchEngineMode::Low:
      return "low";
    case SkyTraqBin::ParameterSearchEngineMode::Mid:
      return "mid";
    case SkyTraqBin::ParameterSearchEngineMode::High:
      return "high";
    case SkyTraqBin::ParameterSearchEngineMode::Full:
      return "full";
    }
    throw invalid_argument("Unrecognised value for ParameterSearchEngineMode");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::ParameterSearchEngineMode);

  string to_string(SkyTraqBin::NavigationMode mode) {
    switch (mode) {
    case SkyTraqBin::NavigationMode::Auto:
      return "automatic";
    case SkyTraqBin::NavigationMode::Pedestrian:
      return "pedestrian";
    case SkyTraqBin::NavigationMode::Car:
      return "car";
    case SkyTraqBin::NavigationMode::Marine:
      return "marine";
    case SkyTraqBin::NavigationMode::Balloon:
      return "balloon";
    case SkyTraqBin::NavigationMode::Airborne:
      return "airborne";
    }
    throw invalid_argument("Unrecognised value for NavigatioMode");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::NavigationMode);

  string to_string(SkyTraqBin::BootStatus bs) {
    switch (bs) {
    case SkyTraqBin::BootStatus::FromFlash:
      return "boot from Flash";
    case SkyTraqBin::BootStatus::FromROM:
      return "boot from ROM due to Flash boot failure";
    }
    throw invalid_argument("Unrecognised value for BootStatus");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::BootStatus);

  string to_string(SkyTraqBin::InterferenceStatus is) {
    switch (is) {
    case SkyTraqBin::InterferenceStatus::Unknown:
      return "unknown";
    case SkyTraqBin::InterferenceStatus::None:
      return "none";
    case SkyTraqBin::InterferenceStatus::Little:
      return "little";
    case SkyTraqBin::InterferenceStatus::Critical:
      return "critical";
    }
    throw invalid_argument("Unrecognised value for InterferenceStatus");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::InterferenceStatus);

  string to_string(SkyTraqBin::FixType ft) {
    switch (ft) {
    case SkyTraqBin::FixType::None:
      return "none";
    case SkyTraqBin::FixType::TwoDimensional:
      return "2D";
    case SkyTraqBin::FixType::ThreeDimensional:
      return "3D";
    case SkyTraqBin::FixType::Differential:
      return "differential";
    }
    throw invalid_argument("Unrecognised value for FixType");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::FixType);

  string to_string(SkyTraqBin::NavigationState ns) {
    switch (ns) {
    case SkyTraqBin::NavigationState::NoFix:
      return "no fix";
    case SkyTraqBin::NavigationState::Predicted:
      return "predicted";
    case SkyTraqBin::NavigationState::TwoDimensional:
      return "2D";
    case SkyTraqBin::NavigationState::ThreeDimensional:
      return "3D";
    case SkyTraqBin::NavigationState::Differential:
      return "differential";
    }
    throw invalid_argument("Unrecognised value for NavigationState");
  }

  ENUM_OSTREAM_OPERATOR(SkyTraqBin::NavigationState);


}; // namespace std

#undef ENUM_OSTREAM_OPERATOR
