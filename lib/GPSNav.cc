/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <stdexcept>
#include "GPSNav.hh"

#define GETTER(type, class, name, field) const type class::name(void) const { return field; }
#define GETTER_RAW(type, class, name, field) const type class::name##_raw(void) const { return field; }
#define GETTER_MOD(type, class, name, code) const type class::name(void) const { \
    errno = 0; std::feclearexcept(FE_ALL_EXCEPT);			\
    type ret = code;							\
    if (errno == EDOM) throw std::domain_error(std::strerror(errno));	\
    if (std::fetestexcept(FE_INVALID)) throw std::domain_error("Domain error"); \
    if (errno == ERANGE) throw std::invalid_argument(std::strerror(errno)); \
    if (std::fetestexcept(FE_DIVBYZERO)) throw std::invalid_argument("Pole error"); \
    if (std::fetestexcept(FE_OVERFLOW)) throw std::overflow_error("Overflow error"); \
    if (std::fetestexcept(FE_UNDERFLOW)) throw std::underflow_error("Underflow error"); \
    return ret;								\
  }
#define GETTER_ITERATORS(ctype, class, name, cont) ctype::iterator class::name##_begin(void) { return cont.begin(); } \
  ctype::iterator class::name##_end(void) { return cont.end(); }\
  ctype::reverse_iterator class::name##_rbegin(void) { return cont.rbegin(); }\
  ctype::reverse_iterator class::name##_rend(void) { return cont.rend(); }\
  ctype::const_iterator class::name##_cbegin(void) const { return cont.cbegin(); }\
  ctype::const_iterator class::name##_cend(void) const { return cont.cend(); }\
  ctype::const_reverse_iterator class::name##_crbegin(void) const { return cont.crbegin(); }\
  ctype::const_reverse_iterator class::name##_crend(void) const { return cont.crend(); }

namespace GPS {

  uint32_t Subframe::extract_tow_count(const bitstream& bits) {
    return bits.extract<uint32_t>(-216, 17);
  }

  uint8_t Subframe::extract_subframe_number(const bitstream& bits) {
    return 1 + (extract_tow_count(bits) + 4) % 5;
  }

  Subframe::Subframe(uint8_t prn, const bitstream& bits) :
    _prn(prn),
    _preamble(bits.extract<uint8_t>(0, 8)),
    _tow_count(bits.extract<uint32_t>(24, 17)),
    _momentum_or_alert_flag(bits.extract<bool>(41, 1)),
    _sync_or_antispoof_flag(bits.extract<bool>(42, 1)),
    _subframe_num(extract_subframe_number(bits))
  {}

  GETTER(uint8_t, Subframe, PRN, _prn);
  GETTER(uint8_t, Subframe, preamble, _preamble);
  GETTER(uint32_t, Subframe, TOW_count, _tow_count);
  GETTER(bool, Subframe, momentum_flag, _momentum_or_alert_flag);
  GETTER(bool, Subframe, alert_flag, _momentum_or_alert_flag);
  GETTER(bool, Subframe, sync_flag, _sync_or_antispoof_flag);
  GETTER(bool, Subframe, antispoof_flag, _sync_or_antispoof_flag);
  GETTER(uint8_t, Subframe, subframe_number, _subframe_num);

  Subframe::ptr parse_subframe(uint8_t prn, const bitstream& bits) {
    if (bits.size() < 25)
      throw std::domain_error("Not enough bytes to determine the subframe number");

    uint8_t subframe_num = Subframe::extract_subframe_number(bits);

    if (subframe_num == 1)
      return std::make_shared<Sat_clock_and_health>(prn, bits);

    if (subframe_num == 2)
      return std::make_shared<Ephemeris1>(prn, bits);

    if (subframe_num == 3)
      return std::make_shared<Ephemeris2>(prn, bits);

    uint8_t page_num = Subframe_4_or_5::extract_page_number(bits);

    if (((subframe_num == 4) && (((page_num >= 2) && (page_num <= 5))
				 || ((page_num >= 7) && (page_num <= 10))))
	|| ((subframe_num == 5) && (page_num >= 1) && (page_num <= 24)))
      return std::make_shared<Almanac>(prn, bits);

    if (subframe_num == 4) {
      if (page_num == 17)
	return std::make_shared<Special_message>(prn, bits);

      if (page_num == 18)
	return std::make_shared<Ionosphere_UTC>(prn, bits);

      if (page_num == 25)
	return std::make_shared<Sat_config>(prn, bits);
    }

    if ((subframe_num == 5) && (page_num == 25))
      return std::make_shared<Sat_health>(prn, bits);

    return std::make_shared<Reserved_and_spare>(prn, bits);
  }


  Sat_clock_and_health::Sat_clock_and_health(uint8_t prn, const bitstream& bits) :
    Subframe(prn, bits)
  {
    _week_num = bits.extract<uint16_t>(48, 10);
    _ura = bits.extract<uint8_t>(60, 4);
    _nav_data_ok = bits.extract<bool>(64, 1);

    auto h = bits.extract<uint8_t>(65, 5);
    switch (h) {
    case 0:
    case 28:
    case 29:
    case 30:
    case 31:
      _health = static_cast<SignalComponentHealth>(h);
      break;

    default:
      _health = SignalComponentHealth::Problems;
    }

    _iodc = (bits.extract<uint16_t>(70, 2) << 8) | bits.extract<uint16_t>(168, 8);
    _t_gd = bits.extract<int8_t>(160, 8);
    _t_oc = bits.extract<uint16_t>(176, 16);
    _a_f2 = bits.extract<int8_t>(192, 8);
    _a_f1 = bits.extract<int16_t>(200, 16);
    _a_f0 = bits.extract<int32_t>(216, 22);
  }

  GETTER(uint16_t, Sat_clock_and_health, week_number, _week_num);
  GETTER(uint8_t, Sat_clock_and_health, URA, _ura);
  GETTER(bool, Sat_clock_and_health, navigation_data_ok, _nav_data_ok);
  GETTER(SignalComponentHealth, Sat_clock_and_health, health, _health);
  GETTER(uint16_t, Sat_clock_and_health, IODC, _iodc);
  GETTER_RAW(int8_t, Sat_clock_and_health, T_GD, _t_gd);
  GETTER_MOD(double, Sat_clock_and_health, T_GD, _t_gd * pow(2, -31));
  GETTER_RAW(uint16_t, Sat_clock_and_health, t_OC, _t_oc);
  GETTER_MOD(uint32_t, Sat_clock_and_health, t_OC, _t_oc * 16);
  GETTER_RAW(int8_t, Sat_clock_and_health, a_f2, _a_f2);
  GETTER_MOD(double, Sat_clock_and_health, a_f2, _a_f2 * pow(2, -55));
  GETTER_RAW(int16_t, Sat_clock_and_health, a_f1, _a_f1);
  GETTER_MOD(double, Sat_clock_and_health, a_f1, _a_f1 * pow(2, -43));
  GETTER_RAW(int32_t, Sat_clock_and_health, a_f0, _a_f0);
  GETTER_MOD(double, Sat_clock_and_health, a_f0, _a_f0 * pow(2, -31));


  Ephemeris1::Ephemeris1(uint8_t prn, const bitstream& bits) :
    Subframe(prn, bits)
  {
    _iode = bits.extract<uint8_t>(48, 8);
    _c_rs = bits.extract<int16_t>(56, 16);
    _delta_n = bits.extract<int16_t>(72, 16);
    _m_0 = bits.extract<int32_t>(88, 32);
    _c_uc = bits.extract<int16_t>(120, 16);
    _e = bits.extract<uint32_t>(136, 32);
    _c_us = bits.extract<int16_t>(168, 16);
    _sqrt_a = bits.extract<uint32_t>(184, 32);
    _t_oe = bits.extract<uint16_t>(216, 16);
  }

  GETTER(uint8_t, Ephemeris1, IODE, _iode);
  GETTER_RAW(int16_t, Ephemeris1, C_rs, _c_rs);
  GETTER_MOD(double, Ephemeris1, C_rs, _c_rs * pow(2, -5));
  GETTER_RAW(int16_t, Ephemeris1, delta_n, _delta_n);
  GETTER_MOD(double, Ephemeris1, delta_n, _delta_n * pow(2, -43));
  GETTER_RAW(int32_t, Ephemeris1, M_0, _m_0);
  GETTER_MOD(double, Ephemeris1, M_0, _m_0 * pow(2, -31));
  GETTER_RAW(int16_t, Ephemeris1, C_uc, _c_uc);
  GETTER_MOD(double, Ephemeris1, C_uc, _c_uc * pow(2, -29));
  GETTER_RAW(uint32_t, Ephemeris1, e, _e);
  GETTER_MOD(double, Ephemeris1, e, _e * pow(2, -33));
  GETTER_RAW(int16_t, Ephemeris1, C_us, _c_us);
  GETTER_MOD(double, Ephemeris1, C_us, _c_us * pow(2, -29));
  GETTER_RAW(uint32_t, Ephemeris1, sqrt_A, _sqrt_a);
  GETTER_MOD(double, Ephemeris1, sqrt_A, _sqrt_a * pow(2, -19));
  GETTER_RAW(uint16_t, Ephemeris1, t_oe, _t_oe);
  GETTER_MOD(uint32_t, Ephemeris1, t_oe, _t_oe << 4);


  Ephemeris2::Ephemeris2(uint8_t prn, const bitstream& bits) :
    Subframe(prn, bits)
  {
    _c_ic = bits.extract<int16_t>(48, 16);
    _omega_0 = bits.extract<int32_t>(64, 32);
    _c_is = bits.extract<int16_t>(96, 16);
    _i_0 = bits.extract<int32_t>(112, 32);
    _c_rc = bits.extract<int16_t>(144, 16);
    _omega = bits.extract<int32_t>(160, 32);
    _omegadot = bits.extract<int32_t>(192, 24);
    _iode = bits.extract<uint8_t>(216, 8);
    _idot = bits.extract<int16_t>(224, 14);
  }

  GETTER_RAW(int16_t, Ephemeris2, C_ic, _c_ic);
  GETTER_MOD(double, Ephemeris2, C_ic, _c_ic * pow(2, -29));
  GETTER_RAW(int32_t, Ephemeris2, OMEGA_0, _omega_0);
  GETTER_MOD(double, Ephemeris2, OMEGA_0, _omega_0 * pow(2, -31));
  GETTER_RAW(int16_t, Ephemeris2, C_is, _c_is);
  GETTER_MOD(double, Ephemeris2, C_is, _c_is * pow(2, -29));
  GETTER_RAW(int32_t, Ephemeris2, i_0, _i_0);
  GETTER_MOD(double, Ephemeris2, i_0, _i_0 * pow(2, -31));
  GETTER_RAW(int16_t, Ephemeris2, C_rc, _c_rc);
  GETTER_MOD(double, Ephemeris2, C_rc, _c_rc * pow(2, -5));
  GETTER_RAW(int32_t, Ephemeris2, omega, _omega);
  GETTER_MOD(double, Ephemeris2, omega, _omega * pow(2, -31));
  GETTER_RAW(int32_t, Ephemeris2, OMEGADOT, _omegadot);
  GETTER_MOD(double, Ephemeris2, OMEGADOT, _omegadot * pow(2, -43));
  GETTER(uint8_t, Ephemeris2, IODE, _iode);
  GETTER_RAW(int16_t, Ephemeris2, IDOT, _idot);
  GETTER_MOD(double, Ephemeris2, IDOT, _idot * pow(2, -43));


  uint8_t Subframe_4_or_5::extract_page_number(const bitstream& bits) {
    uint8_t subframe_num = extract_subframe_number(bits);
    uint32_t tow_count = extract_tow_count(bits);
    return 1 + ((tow_count - subframe_num + 1) / 20) % 25;
  }

  Subframe_4_or_5::Subframe_4_or_5(uint8_t prn, const bitstream& bits) :
    Subframe(prn, bits)
  {
    _page_num = extract_page_number(bits);
    _data_id = bits.extract<uint8_t>(48, 2);
    _sat_id = bits.extract<uint8_t>(50, 2);
  }

  GETTER(uint8_t, Subframe_4_or_5, page_number, _page_num);
  GETTER(uint8_t, Subframe_4_or_5, data_id, _data_id);
  GETTER(uint8_t, Subframe_4_or_5, satellite_id, _sat_id);


  Almanac::Almanac(uint8_t prn, const bitstream& bits) :
    Subframe_4_or_5(prn, bits)
  {
    _e = bits.extract<uint16_t>(56);
    _t_oa = bits.extract<uint8_t>(72);
    _sigma_i = bits.extract<int16_t>(80);
    _omegadot = bits.extract<int16_t>(96);
    _nav_data_ok = bits.extract<bool>(113);

    auto h = bits.extract<uint8_t>(114, 5);
    switch (h) {
    case 0:
    case 28:
    case 29:
    case 30:
    case 31:
      _health = static_cast<SignalComponentHealth>(h);
      break;

    default:
      _health = SignalComponentHealth::Problems;
    }

    _sqrt_a = bits.extract<int32_t>(120, 24);
    _omega_0 = bits.extract<int32_t>(144, 24);
    _omega = bits.extract<int32_t>(168, 24);
    _m_0 = bits.extract<int32_t>(192, 24);
    _a_f0 = bits.extract<int16_t>(216, 11);
    _a_f1 = bits.extract<int16_t>(227, 11);
  }

  GETTER_RAW(uint16_t, Almanac, e, _e);
  GETTER_MOD(double, Almanac, e, _e * pow(2, -21));
  GETTER_RAW(uint8_t, Almanac, t_oa, _t_oa);
  GETTER_MOD(double,Almanac,  t_oa, _t_oa * pow(2, 12));
  GETTER_RAW(int16_t, Almanac, sigma_i, _sigma_i);
  GETTER_MOD(double, Almanac, sigma_i, _sigma_i * pow(2, -19));
  GETTER_RAW(int16_t, Almanac, OMEGADOT, _omegadot);
  GETTER_MOD(double, Almanac, OMEGADOT, _omegadot * pow(2, -38));
  GETTER(bool, Almanac, navigation_data_ok, _nav_data_ok);
  GETTER(SignalComponentHealth, Almanac, health, _health);
  GETTER_RAW(int32_t, Almanac, sqrt_A, _sqrt_a);
  GETTER_MOD(double, Almanac, sqrt_A, _sqrt_a * pow(2, -11));
  GETTER_RAW(int32_t, Almanac, OMEGA_0, _omega_0);
  GETTER_MOD(double, Almanac, OMEGA_0, _omega_0 * pow(2, -23));
  GETTER_RAW(int32_t, Almanac, omega, _omega);
  GETTER_MOD(double, Almanac, omega, _omega * pow(2, -23));
  GETTER_RAW(int32_t, Almanac, M_0, _m_0);
  GETTER_MOD(double, Almanac, M_0, _m_0 * pow(2, -23));
  GETTER_RAW(int16_t, Almanac, a_f0, _a_f0);
  GETTER_MOD(double, Almanac, a_f0, _a_f0 * pow(2, -20));
  GETTER_RAW(int16_t, Almanac, a_f1, _a_f1);
  GETTER_MOD(double, Almanac, a_f1, _a_f1 * pow(2, -38));


  Special_message::Special_message(uint8_t prn, const bitstream& bits) :
    Subframe_4_or_5(prn, bits)
  {
    for (uint8_t i = 0; i < 22; i++)
      _msg[i] = bits.extract<char>((i * 8) + 56);
  }

  std::string Special_message::contents(void) const {
    std::string m;
    for (uint8_t i = 0; i < 22; i++)
      if (_msg[i] == 0370)
	m += "�";
      else
	m += _msg[i];

    return m;
  }


  Ionosphere_UTC::Ionosphere_UTC(uint8_t prn, const bitstream& bits) :
    Subframe_4_or_5(prn, bits)
  {
    _alpha_0 = bits.extract<int8_t>(56, 8);
    _alpha_1 = bits.extract<int8_t>(64, 8);
    _alpha_2 = bits.extract<int8_t>(72, 8);
    _alpha_3 = bits.extract<int8_t>(80, 8);
    _beta_0 = bits.extract<int8_t>(88, 8);
    _beta_1 = bits.extract<int8_t>(96, 8);
    _beta_2 = bits.extract<int8_t>(104, 8);
    _beta_3 = bits.extract<int8_t>(112, 8);
    _a_0 = bits.extract<int32_t>(120, 32);
    _a_1 = bits.extract<int32_t>(152, 24);
    _delta_t_ls = bits.extract<uint8_t>(176, 8);
    _t_ot = bits.extract<uint8_t>(184, 8);
    _wn_t = bits.extract<uint8_t>(192, 8);
    _wn_lsf = bits.extract<uint8_t>(200, 8);
    _dn = bits.extract<uint8_t>(208, 8);
    _delta_t_lsf = bits.extract<int8_t>(216, 8);
  }

  GETTER_RAW(int8_t, Ionosphere_UTC, alpha_0, _alpha_0);
  GETTER_MOD(double, Ionosphere_UTC, alpha_0, _alpha_0 * pow(2, -30));
  GETTER_RAW(int8_t, Ionosphere_UTC, alpha_1, _alpha_1);
  GETTER_MOD(double, Ionosphere_UTC, alpha_1, _alpha_1 * pow(2, -27));
  GETTER_RAW(int8_t, Ionosphere_UTC, alpha_2, _alpha_2);
  GETTER_MOD(double, Ionosphere_UTC, alpha_2, _alpha_2 * pow(2, -24));
  GETTER_RAW(int8_t, Ionosphere_UTC, alpha_3, _alpha_3);
  GETTER_MOD(double, Ionosphere_UTC, alpha_3, _alpha_3 * pow(2, -24));
  GETTER_RAW(int8_t, Ionosphere_UTC, beta_0, _beta_0);
  GETTER_MOD(int32_t, Ionosphere_UTC, beta_0, _beta_0 << 11);
  GETTER_RAW(int8_t, Ionosphere_UTC, beta_1, _beta_1);
  GETTER_MOD(int32_t, Ionosphere_UTC, beta_1, _beta_1 << 14);
  GETTER_RAW(int8_t, Ionosphere_UTC, beta_2, _beta_2);
  GETTER_MOD(int32_t, Ionosphere_UTC, beta_2, _beta_2 << 16);
  GETTER_RAW(int8_t, Ionosphere_UTC, beta_3, _beta_3);
  GETTER_MOD(int32_t, Ionosphere_UTC, beta_3, _beta_3 << 16);
  GETTER_RAW(int32_t, Ionosphere_UTC, A_0, _a_0);
  GETTER_MOD(double, Ionosphere_UTC, A_0, _a_0 * pow(2, -30));
  GETTER_RAW(int32_t, Ionosphere_UTC, A_1, _a_1);
  GETTER_MOD(double, Ionosphere_UTC, A_1, _a_1 * pow(2, -50));
  GETTER(uint8_t, Ionosphere_UTC, delta_t_LS, _delta_t_ls);
  GETTER_RAW(uint8_t, Ionosphere_UTC, t_ot, _t_ot);
  GETTER_MOD(uint32_t, Ionosphere_UTC, t_ot, _t_ot << 12);
  GETTER(uint8_t, Ionosphere_UTC, WN_t, _wn_t);
  GETTER(uint8_t, Ionosphere_UTC, WN_LSF, _wn_lsf);
  GETTER(uint8_t, Ionosphere_UTC, DN, _dn);
  GETTER(int8_t, Ionosphere_UTC, delta_t_LSF, _delta_t_lsf);


  Sat_config::Sat_config(uint8_t prn, const bitstream& bits) :
    Subframe_4_or_5(prn, bits),
    _configs(32),
    _nav_data_okays(8),
    _healths(8)
  {
    for (uint8_t i = 0; i < 32; i++)
      _configs[i] = SatelliteConfig(bits.extract<uint8_t>(57 + (i * 4), 3));

    for (uint8_t i = 0; i < 8; i++) {
      _nav_data_okays[i] = bits.extract<bool>(186 + (i * 6));

      auto h = bits.extract<uint8_t>(187 + (i * 6), 5);
      switch (h) {
      case 0:
      case 28:
      case 29:
      case 30:
      case 31:
	_healths[i] = static_cast<SignalComponentHealth>(h);
	break;

      default:
	_healths[i] = SignalComponentHealth::Problems;
      }
    }
  }

  GETTER_ITERATORS(std::vector<SatelliteConfig>, Sat_config, configs, _configs);
  GETTER_ITERATORS(std::vector<bool>, Sat_config, navigation_data_okays, _nav_data_okays);
  GETTER_ITERATORS(std::vector<SignalComponentHealth>, Sat_config, healths, _healths);

  const SatelliteConfig Sat_config::config(uint8_t i) const {
    return _configs.at(i - 1);
  }

  const bool Sat_config::navigation_data_ok(uint8_t i) const {
    return _nav_data_okays.at(i - 25);
  }

  const SignalComponentHealth Sat_config::health(uint8_t i) const {
    return _healths.at(i - 25);
  }


  Sat_health::Sat_health(uint8_t prn, const bitstream& bits) :
    Subframe_4_or_5(prn, bits),
    _nav_data_okays(24),
    _healths(24)
  {
    _t_oa = bits.extract<uint8_t>(56);
    _wn_a = bits.extract<uint8_t>(64);

    for (uint8_t i = 0; i < 24; i++) {
      _nav_data_okays[i] = bits.extract<bool>(72 + (i * 6));

      auto h = bits.extract<uint8_t>(73 + (i * 6), 5);
      switch (h) {
      case 0:
      case 28:
      case 29:
      case 30:
      case 31:
	_healths[i] = static_cast<SignalComponentHealth>(h);
	break;

      default:
	_healths[i] = SignalComponentHealth::Problems;
      }
    }
  }

  GETTER(uint8_t, Sat_health, t_oa, _t_oa);
  GETTER(uint8_t, Sat_health, WN_a, _wn_a);
  GETTER_ITERATORS(std::vector<bool>, Sat_health, navigation_data_okays, _nav_data_okays);
  GETTER_ITERATORS(std::vector<SignalComponentHealth>, Sat_health, healths, _healths);

  const bool Sat_health::navigation_data_ok(uint8_t i) const {
    return _nav_data_okays.at(i - 1);
  }

  const SignalComponentHealth Sat_health::health(uint8_t i) const {
    return _healths.at(i - 1);
  }


}; // namespace GPS

#define ENUM_OSTREAM_OPERATOR(type) std::ostream& operator<< (std::ostream& out, type val) { out << std::to_string(val); return out;  }

namespace std {

  std::string to_string(GPS::SignalComponentHealth health) {
    switch (health) {
    case GPS::SignalComponentHealth::All_ok:
      return "all signals ok";
    case GPS::SignalComponentHealth::Is_temporarily_out:
      return "satellite is temporarily out";
    case GPS::SignalComponentHealth::Will_be_temporarily_out:
      return "satellite will be temporarily out";
    case GPS::SignalComponentHealth::Spare:
      return "spare";
    case GPS::SignalComponentHealth::Bad:
      return "more than one combination would be required to describe anomolies";
    case GPS::SignalComponentHealth::Problems:
      return "satellite is experiencing code modulation and/or signal power level transmission problems";
    }
    throw invalid_argument("Unrecognised value for StartMode");
  }

  ENUM_OSTREAM_OPERATOR(GPS::SignalComponentHealth);

}; // namespace std

#undef GETTER
#undef GETTER_RAW
#undef GETTER_MOD
#undef GETTER_ITERATORS
#undef ENUM_OSTREAM_OPERATOR
