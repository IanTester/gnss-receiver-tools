/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SkyTraqBin.hh"
#include "BE.hh"
#include "LE.hh"

/*
  Sources:
  https://store-lgdi92x.mybigcommerce.com/content/AN0028_1.4.31.pdf	(Binary messages of Skytraq Venus 8)
  https://store-lgdi92x.mybigcommerce.com/content/AN0024_v07.pdf	(Raw measurement binary messages of Skytraq 6 & 8)
  https://store-lgdi92x.mybigcommerce.com/content/SUP800F_v0.6.pdf	(Skytraq SUP800F datasheet)
  https://store-lgdi92x.mybigcommerce.com/content/AN0008_v1.4.17.pdf    (Datalogging extension for Venus 8)
*/

#define GETTER(type, class, name, field) const type class::name(void) const { return field; }

#define GETTER_SETTER(type, class, name, field) const type class::name(void) const { return field; } \
  void class::set_##name(type val) { field = val; }

#define SETTER_BOOL(class, name, field) void class::set_##name(bool val) { field = val; } \
  void class::unset_##name(void) { field = false; }

#define GETTER_RAW(type, class, name, field) const type class::name##_raw(void) const { return field; }

#define GETTER_SETTER_RAW(type, class, name, field) const type class::name##_raw(void) const { return field; } \
  void class::set_##name##_raw(type val) { field = val; }

#define GETTER_MOD(type, class, name, code) const type class::name(void) const { return code; }

#define GETTER_SETTER_MOD(type, class, name, field, code_get, code_set) const type class::name(void) const { return code_get; } \
  void class::set_##name(type val) { field = code_set; }

#define GETTER_ITERATORS(ctype, class, name, cont) ctype::iterator class::name##_begin(void) { return cont.begin(); } \
  ctype::iterator class::name##_end(void) { return cont.end(); }\
  ctype::reverse_iterator class::name##_rbegin(void) { return cont.rbegin(); }\
  ctype::reverse_iterator class::name##_rend(void) { return cont.rend(); }\
  ctype::const_iterator class::name##_cbegin(void) const { return cont.cbegin(); }\
  ctype::const_iterator class::name##_cend(void) const { return cont.cend(); }\
  ctype::const_reverse_iterator class::name##_crbegin(void) const { return cont.crbegin(); }\
  ctype::const_reverse_iterator class::name##_crend(void) const { return cont.crend(); }


namespace SkyTraqBin {

  Sw_ver::Sw_ver(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _sw_type = static_cast<SwType>(extract_be<uint8_t>(payload));
    _kernel_ver = extract_be<uint32_t>(payload);
    _odm_ver = extract_be<uint32_t>(payload);
    _revision = greg::date(payload[1] + 1900, payload[2], payload[3]);
    payload += 4;
  }


  Sw_CRC::Sw_CRC(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _sw_type = static_cast<SwType>(extract_be<uint8_t>(payload));
    _crc = extract_be<uint16_t>(payload);
  }


  Ack::Ack(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _has_subid(false),
    _ack_subid(0)
  {
    _ack_id = extract_be<uint8_t>(payload);
    if (payload != payload_last) {
      _has_subid = true;
      _ack_subid = extract_be<uint8_t>(payload);
    }
  }


  Nack::Nack(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _has_subid(false),
    _nack_subid(0)
  {
    _nack_id = extract_be<uint8_t>(payload);
    if (payload.base() != payload_last.base()) {
      _has_subid = true;
      _nack_subid = extract_be<uint8_t>(payload);
    }
  }


  Pos_update_rate::Pos_update_rate(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _update_rate = extract_be<uint8_t>(payload);
  }


  GPS_almanac_data::GPS_almanac_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _words(64)
  {
    _prn = extract_be<uint8_t>(payload);

    for (int i = 0; i < 8; i++)

      _words.insert(extract_be24(payload), i * 24);
    _week_num = extract_be<int16_t>(payload);
  }


  Bin_measurement_data_output_status::Bin_measurement_data_output_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _output_rate = static_cast<OutputRate>(extract_be<uint8_t>(payload));
    _meas_time = extract_be<uint8_t>(payload);
    _raw_meas = extract_be<uint8_t>(payload);
    _sv_ch_status = extract_be<uint8_t>(payload);
    _rcv_state = extract_be<uint8_t>(payload);
    uint8_t flags = extract_be<uint8_t>(payload);
    _sub_gps = flags & 0x01;
    _sub_glonass = flags & 0x02;
    _sub_galileo = flags & 0x04;
    _sub_beidou2 = flags & 0x08;
  }


  Glonass_ephemeris_data::Glonass_ephemeris_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _string1(80), _string2(80), _string3(80), _string4(80)
  {
    _slot_number = extract_be<uint8_t>(payload);
    _k_number = static_cast<int8_t>(extract_be<uint8_t>(payload));

    for (int i = 0; i < 10; i++)
      _string1.insert(extract_be<uint8_t>(payload), i * 8);

    for (int i = 0; i < 10; i++)
      _string2.insert(extract_be<uint8_t>(payload), i * 8);

    for (int i = 0; i < 10; i++)
      _string3.insert(extract_be<uint8_t>(payload), i * 8);

    for (int i = 0; i < 10; i++)
      _string4.insert(extract_be<uint8_t>(payload), i * 8);
}


  NMEA_talker_ID::NMEA_talker_ID(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _talker_id = static_cast<TalkerID>(extract_be<uint8_t>(payload));
  }


  Log_status_output::Log_status_output(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _wr_ptr = extract_le<uint32_t>(payload);
    _sectors_left = extract_le<uint16_t>(payload);
    _total_sectors = extract_le<uint16_t>(payload);
    _max_time = extract_le<uint32_t>(payload);
    _min_time = extract_le<uint32_t>(payload);
    _max_dist = extract_le<uint32_t>(payload);
    _min_dist = extract_le<uint32_t>(payload);
    _max_speed = extract_le<uint32_t>(payload);
    _min_speed = extract_le<uint32_t>(payload);
    _datalog = extract_le<bool>(payload);
    _fifo_mode = extract_be<uint8_t>(payload);
  }


  Nav_data_msg::Nav_data_msg(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _fix_type = static_cast<FixType>(extract_be<uint8_t>(payload));
    _num_sv = extract_be<uint8_t>(payload);
    _week_num = extract_be<uint16_t>(payload);
    _tow = extract_be<uint32_t>(payload);
    _lat = extract_be<int32_t>(payload);
    _lon = extract_be<int32_t>(payload);
    _e_alt = extract_be<int32_t>(payload);
    _alt = extract_be<int32_t>(payload);
    _gdop = extract_be<uint16_t>(payload);
    _pdop = extract_be<uint16_t>(payload);
    _hdop = extract_be<uint16_t>(payload);
    _vdop = extract_be<uint16_t>(payload);
    _tdop = extract_be<uint16_t>(payload);
    _ecef_x = extract_be<int32_t>(payload);
    _ecef_y = extract_be<int32_t>(payload);
    _ecef_z = extract_be<int32_t>(payload);
    _ecef_vx = extract_be<int32_t>(payload);
    _ecef_vy = extract_be<int32_t>(payload);
    _ecef_vz = extract_be<int32_t>(payload);
  }

  GETTER(FixType, Nav_data_msg, fix_type, _fix_type);
  GETTER(uint8_t, Nav_data_msg, num_sv, _num_sv);
  GETTER(uint16_t, Nav_data_msg, week_number, _week_num);

  GETTER_MOD(double, Nav_data_msg, time_of_week, _tow * 1e-02);
  GETTER_RAW(uint32_t, Nav_data_msg, time_of_week, _tow);

  GETTER_MOD(double, Nav_data_msg, lat, _lat * 1e-7);
  GETTER_RAW(int32_t, Nav_data_msg, lat, _lat);

  GETTER_MOD(double, Nav_data_msg, lon, _lon * 1e-7);
  GETTER_RAW(int32_t, Nav_data_msg, lon, _lon);

  GETTER_MOD(double, Nav_data_msg, ellipsoid_alt, _e_alt * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ellipsoid_alt, _e_alt);

  GETTER_MOD(double, Nav_data_msg, alt, _alt * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, alt, _alt);

  GETTER_MOD(double, Nav_data_msg, GDOP, _gdop * 0.01);
  GETTER_RAW(uint16_t, Nav_data_msg, GDOP, _gdop);

  GETTER_MOD(double, Nav_data_msg, PDOP, _pdop * 0.01);
  GETTER_RAW(uint16_t, Nav_data_msg, PDOP, _pdop);

  GETTER_MOD(double, Nav_data_msg, HDOP, _hdop * 0.01);
  GETTER_RAW(uint16_t, Nav_data_msg, HDOP, _hdop);

  GETTER_MOD(double, Nav_data_msg, VDOP, _vdop * 0.01);
  GETTER_RAW(uint16_t, Nav_data_msg, VDOP, _vdop);

  GETTER_MOD(double, Nav_data_msg, TDOP, _tdop * 0.01);
  GETTER_RAW(uint16_t, Nav_data_msg, TDOP, _tdop);

  GETTER_MOD(double, Nav_data_msg, ECEF_X, _ecef_x * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ECEF_X, _ecef_x);

  GETTER_MOD(double, Nav_data_msg, ECEF_Y, _ecef_y * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ECEF_Y, _ecef_y);

  GETTER_MOD(double, Nav_data_msg, ECEF_Z, _ecef_z * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ECEF_Z, _ecef_z);

  GETTER_MOD(double, Nav_data_msg, ECEF_VX, _ecef_vx * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ECEF_VX, _ecef_vx);

  GETTER_MOD(double, Nav_data_msg, ECEF_VY, _ecef_vy * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ECEF_VY, _ecef_vy);

  GETTER_MOD(double, Nav_data_msg, ECEF_VZ, _ecef_vz * 0.01);
  GETTER_RAW(int32_t, Nav_data_msg, ECEF_VZ, _ecef_vz);


  GNSS_datum::GNSS_datum(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _datum_index = extract_be<uint16_t>(payload);
  }


  GNSS_DOP_mask::GNSS_DOP_mask(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _dop_mode = static_cast<DOPmode>(extract_be<uint8_t>(payload));
    _pdop = extract_be<int16_t>(payload);
    _hdop = extract_be<int16_t>(payload);
    _gdop = extract_be<int16_t>(payload);
  }


  GNSS_elevation_CNR_mask::GNSS_elevation_CNR_mask(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _mode_select = static_cast<ElevationCNRmode>(extract_be<uint8_t>(payload));
    _el_mask = extract_be<uint8_t>(payload);
    _cnr_mask = extract_be<uint8_t>(payload);
  }


  GPS_ephemeris_data::GPS_ephemeris_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _subframe1(224), _subframe2(224), _subframe3(224)
  {
    _sv_num = extract_be<uint16_t>(payload);

    for (int i = 0; i < 28; i++)
      _subframe1.insert(extract_be<uint8_t>(payload), i * 8);

    for (int i = 0; i < 28; i++)
      _subframe2.insert(extract_be<uint8_t>(payload), i * 8);

    for (int i = 0; i < 28; i++)
      _subframe3.insert(extract_be<uint8_t>(payload), i * 8);
}


  GNSS_pos_pinning_status::GNSS_pos_pinning_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _status = static_cast<DefaultOrEnable>(extract_be<uint8_t>(payload));
    _pin_speed = extract_be<uint16_t>(payload);
    _pin_count = extract_be<uint16_t>(payload);
    _unpin_speed = extract_be<uint16_t>(payload);
    _unpin_count = extract_be<uint16_t>(payload);
    _unpin_dist = extract_be<uint16_t>(payload);
  }


  GNSS_power_mode_status::GNSS_power_mode_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _power_mode = static_cast<PowerMode>(extract_be<uint8_t>(payload));
  }


  GNSS_1PPS_cable_delay::GNSS_1PPS_cable_delay(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _delay = extract_be<int32_t>(payload);
  }


  GNSS_1PPS_timing::GNSS_1PPS_timing(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _timing_mode = static_cast<SkyTraq::TimingMode>(extract_be<uint8_t>(payload));
    _survey_len = extract_be<uint32_t>(payload);
    _std_dev = extract_be<uint32_t>(payload);
    _lat = extract_be<double>(payload);
    _lon = extract_be<double>(payload);
    _alt = extract_be<float>(payload);
    _curr_timing_mode = static_cast<SkyTraq::TimingMode>(extract_be<uint8_t>(payload));
    _curr_survey_len = extract_be<uint32_t>(payload);
  }


  Measurement_time::Measurement_time(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _issue = extract_be<uint8_t>(payload);
    _weeknumber = extract_be<uint16_t>(payload);
    _time_in_week = extract_be<uint32_t>(payload);
    _period = extract_be<uint16_t>(payload);
  }

  GETTER(uint8_t, Measurement_time, issue_of_data, _issue);
  GETTER(uint16_t, Measurement_time, week_number, _weeknumber);
  GETTER(uint32_t, Measurement_time, time_in_week, _time_in_week);
  GETTER(uint16_t, Measurement_time, period, _period);


  RawMeasurement::RawMeasurement(uint8_t p, uint8_t c, double pr, double cph, float df, bool hpr, bool hdf, bool hcp, bool wcs, bool cit) :
    PRN(p), CN0(c), pseudo_range(pr), carrier_phase(cph), doppler_freq(df),
    has_pseudo_range(hpr), has_doppler_freq(hdf), has_carrier_phase(hcp), will_cycle_slip(wcs), coherent_integration_time(cit)
  {}


  Raw_measurements::Raw_measurements(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _issue = extract_be<uint8_t>(payload);
    _num_meas = extract_be<uint8_t>(payload);

    _measurements.reserve((payload_last - payload + 1) / 23);
    while (payload + 23 <= payload_last) {
      uint8_t p = extract_be<uint8_t>(payload);
      uint8_t c = extract_be<uint8_t>(payload);
      double pr = extract_be<double>(payload);
      double cph = extract_be<double>(payload);
      float df = extract_be<float>(payload);
      uint8_t ci = extract_be<uint8_t>(payload);

      _measurements.push_back(RawMeasurement(p, c, pr, cph, df,
					     ci & 0x01, ci & 0x02, ci & 0x04, ci & 0x08, ci & 0x10));
    }
  }

  GETTER(uint8_t, Raw_measurements, issue_of_data, _issue);
  GETTER(uint8_t, Raw_measurements, num_measurements, _num_meas);
  GETTER(std::vector<RawMeasurement>&, Raw_measurements, measurements, _measurements);

  const RawMeasurement Raw_measurements::measurement(uint8_t i) const {
    return _measurements[i];
  }


  SvStatus::SvStatus(uint8_t cid, uint8_t p, bool sha, bool she, bool sih, uint8_t u, int8_t c, int16_t el, int16_t az, bool chpi, bool chbs, bool chfs, bool che, bool cfn, bool cfd) :
    channel_id(cid), PRN(p),
    sv_has_almanac(sha), sv_has_ephemeris(she), sv_is_healthy(sih),
    URA(u), CN0(c), elevation(el), azimuth(az),
    chan_has_pull_in(chpi), chan_has_bit_sync(chbs), chan_has_frame_sync(chfs), chan_has_ephemeris(che), chan_for_normal(cfn), chan_for_differential(cfd)
  {}


  SV_channel_status::SV_channel_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _issue = extract_be<uint8_t>(payload);
    _num_sv = extract_be<uint8_t>(payload);

    _statuses.reserve((payload_last - payload + 1) / 10);
    while (payload + 10 <= payload_last) {
      uint8_t cid = extract_be<uint8_t>(payload);
      uint8_t p = extract_be<uint8_t>(payload);
      uint8_t sv_status = extract_be<uint8_t>(payload);
      uint8_t u = extract_be<uint8_t>(payload);
      int8_t c = extract_be<int8_t>(payload);
      int16_t el = extract_be<int16_t>(payload);
      int16_t az = extract_be<int16_t>(payload);
      uint8_t chan_status = extract_be<uint8_t>(payload);

      _statuses.push_back(SvStatus(cid, p,
				   sv_status & 0x01, sv_status & 0x02, sv_status & 0x04,
				   u, c, el, az,
				   chan_status & 0x01, chan_status & 0x02,
				   chan_status & 0x04, chan_status & 0x08,
				   chan_status & 0x10, chan_status & 0x20));
    }
  }

  GETTER(uint8_t, SV_channel_status, issue_of_data, _issue);
  GETTER(uint8_t, SV_channel_status, num_svs, _num_sv);
  GETTER(std::vector<SvStatus>&, SV_channel_status, statuses, _statuses);

  const SvStatus SV_channel_status::status(uint8_t i) const {
    return _statuses[i];
  }


  Rcv_state::Rcv_state(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _issue = extract_be<uint8_t>(payload);
    _nav_state = static_cast<NavigationState>(extract_be<uint8_t>(payload));
    _weeknum = extract_be<uint16_t>(payload);
    _tow = extract_be<double>(payload);
    _ecef_x = extract_be<double>(payload);
    _ecef_y = extract_be<double>(payload);
    _ecef_z = extract_be<double>(payload);
    _ecef_vx = extract_be<float>(payload);
    _ecef_vy = extract_be<float>(payload);
    _ecef_vz = extract_be<float>(payload);
    _clock_bias = extract_be<double>(payload);
    _clock_drift = extract_be<float>(payload);
    _gdop = extract_be<float>(payload);
    _pdop = extract_be<float>(payload);
    _hdop = extract_be<float>(payload);
    _vdop = extract_be<float>(payload);
    _tdop = extract_be<float>(payload);
  }


  GPS_subframe_data::GPS_subframe_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload)
  {
    _prn = extract_be<uint8_t>(payload);
    _subframe_num = extract_be<uint8_t>(payload);

    _num_bits = 0;
    _bytes.reserve(30);
    for (int i = 0; i < 30; i++) {
      _bytes.push_back(extract_be<uint8_t>(payload));
      _num_bits += 8;
    }
  }

  GETTER(uint8_t, GPS_subframe_data, PRN, _prn);
  GETTER(uint8_t, GPS_subframe_data, subframe_num, _subframe_num);
  GETTER(uint8_t*, GPS_subframe_data, bytes, _bytes.data());

  const uint8_t GPS_subframe_data::byte(int i) const {
    return _bytes.at(i);
  }




  Glonass_string_data::Glonass_string_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _bytes(72)
  {
    _svid = extract_be<uint8_t>(payload);
    _string = extract_be<uint8_t>(payload);

    for (int i = 0; i < 9; i++)
      _bytes.insert(extract_be<uint8_t>(payload), i * 8);
  }


  Beidou2_subframe_data::Beidou2_subframe_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    _words(224)
  {
    _d = _msg_id - 0xE0;
    _svid = extract_be<uint8_t>(payload);
    _subframe_num = extract_be<uint8_t>(payload);

    for (uint8_t i = 0; i < 28; i++)
      _words.insert(extract_be<uint8_t>(payload), i * 8);
  }


  /**************************
   * Messages with a sub-ID *
   **************************/


  Output_message_with_subid::Output_message_with_subid(std::vector<uint8_t>::const_iterator& payload,
						       const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message(payload),
    with_subid(payload == payload_last ? 0 : extract_be<uint8_t>(payload))
  {}

  GNSS_SBAS_status::GNSS_SBAS_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _enabled = extract_be<bool>(payload);
    _ranging = static_cast<EnableOrAuto>(extract_be<uint8_t>(payload));
    _ranging_ura_mask = extract_be<uint8_t>(payload);
    _correction = extract_be<bool>(payload);
    _num_channels = extract_be<uint8_t>(payload);

    uint8_t sub_mask = extract_be<uint8_t>(payload);
    _waas = sub_mask & 0x01;
    _egnos = sub_mask & 0x02;
    _msas = sub_mask & 0x04;
    _all_sbas = sub_mask & 0x80;
  }


  GNSS_QZSS_status::GNSS_QZSS_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _enabled = static_cast<bool>(extract_be<uint8_t>(payload));
    _num_channels = extract_be<uint8_t>(payload);
  }


  GNSS_SAEE_status::GNSS_SAEE_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _enabled = static_cast<DefaultOrEnable>(extract_be<uint8_t>(payload));
  }


  GNSS_boot_status::GNSS_boot_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _status = static_cast<BootStatus>(extract_be<uint8_t>(payload));

    uint8_t flash_type = extract_be<uint8_t>(payload);
    _winbond = flash_type & 0x01;
    _eon = flash_type & 0x02;
    _parallel = flash_type & 0x04;
  }


  GNSS_extended_NMEA_msg_interval::GNSS_extended_NMEA_msg_interval(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _gga = extract_be<uint8_t>(payload);
    _gsa = extract_be<uint8_t>(payload);
    _gsv = extract_be<uint8_t>(payload);
    _gll = extract_be<uint8_t>(payload);
    _rmc = extract_be<uint8_t>(payload);
    _vtg = extract_be<uint8_t>(payload);
    _zda = extract_be<uint8_t>(payload);
    _gns = extract_be<uint8_t>(payload);
    _gbs = extract_be<uint8_t>(payload);
    _grs = extract_be<uint8_t>(payload);
    _dtm = extract_be<uint8_t>(payload);
    _gst = extract_be<uint8_t>(payload);
  }


  GNSS_interference_detection_status::GNSS_interference_detection_status(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _enabled = static_cast<bool>(extract_be<uint8_t>(payload));
    _status = static_cast<InterferenceStatus>(extract_be<uint8_t>(payload));
  }


  GPS_param_search_engine_num::GPS_param_search_engine_num(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _pse_mode = static_cast<ParameterSearchEngineMode>(extract_be<uint8_t>(payload));
  }


  GNSS_nav_mode::GNSS_nav_mode(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _mode = static_cast<NavigationMode>(extract_be<uint8_t>(payload));
  }


  GNSS_constellation_type::GNSS_constellation_type(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    uint16_t mode = extract_be<uint16_t>(payload);
    _gps = mode & 0x0001;
    _glonass = mode & 0x0002;
    _galileo = mode & 0x0004;
    _beidou = mode & 0x0008;
  }


  GNSS_time::GNSS_time(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _tow_ms = extract_be<uint32_t>(payload);
    _tow_ns = extract_be<uint32_t>(payload);
    _week_num = extract_be<uint16_t>(payload);
    _def_leap_secs = extract_be<uint8_t>(payload);
    _curr_leap_secs = extract_be<uint8_t>(payload);

    uint8_t valid = extract_be<uint8_t>(payload);
    _tow_valid = valid & 0x01;
    _wn_valid = valid & 0x02;
    _ls_valid = valid & 0x04;
  }

  GNSS_datum_index::GNSS_datum_index(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _datum_index = extract_be<uint16_t>(payload);
  }

  GNSS_1PPS_pulse_width::GNSS_1PPS_pulse_width(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _width = extract_be<uint32_t>(payload);
  }


  GNSS_1PPS_freq_output::GNSS_1PPS_freq_output(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _frequency = extract_be<uint32_t>(payload);
  }


  Sensor_data::Sensor_data(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
    Output_message_with_subid(payload, payload_last)
  {
    _gx = extract_be<float>(payload);
    _gy = extract_be<float>(payload);
    _gz = extract_be<float>(payload);
    _mx = extract_be<float>(payload);
    _my = extract_be<float>(payload);
    _mz = extract_be<float>(payload);
    _pres = extract_be<uint32_t>(payload);
    _temp = extract_be<float>(payload);
  }

  GETTER(float, Sensor_data, Gx, _gx);
  GETTER(float, Sensor_data, Gy, _gy);
  GETTER(float, Sensor_data, Gz, _gz);

  GETTER_MOD(float, Sensor_data, Mx, _mx * 1e-6);
  GETTER_RAW(float, Sensor_data, Mx, _mx);
  GETTER_MOD(float, Sensor_data, My, _my * 1e-6);
  GETTER_RAW(float, Sensor_data, My, _my);
  GETTER_MOD(float, Sensor_data, Mz, _mz * 1e-6);
  GETTER_RAW(float, Sensor_data, Mz, _mz);

  GETTER(uint32_t, Sensor_data, pressure, _pres);
  GETTER(float, Sensor_data, temperature, _temp);


}; // namespace SkyTraqBin

#undef GETTER
#undef GETTER_SETTER
#undef SETTER_BOOL
#undef GETTER_RAW
#undef GETTER_SETTER_RAW
#undef GETTER_MOD
#undef GETTER_SETTER_MOD
#undef GETTER_ITERATORS
