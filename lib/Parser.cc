/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <algorithm>
#include <thread>
#include <sys/stat.h>
#include "Parser.hh"

namespace GNSS {

  Parser::Parser()
  {}

  void Parser::reset_buffer(void) {
    _parse_buffer.clear();
  }

  void Parser::add_bytes(const std::vector<uint8_t>& buffer) {
    for (auto b : buffer)
      _parse_buffer.push_back(b);
  }

  std::vector<Message::ptr> Parser::parse_messages(void) {
    std::vector<Message::ptr> messages;

    std::size_t end;	// actually the start of the next message
    do {
      end = 0;
      for (std::size_t i = 0; i < _parse_buffer.size() - 1; i++)
	if ((_parse_buffer[i] == 0x0d)
	    && (_parse_buffer[i + 1] == 0x0a)) {
	  end = i + 2;
	  break;
	}
      if (end == 0)
	break;

      std::exception_ptr exp = nullptr;
      try {
	if (_parse_buffer[0] == '$') {
	  std::string line;
	  for (std::size_t i = 0; i < end - 2; i++)
	    line += _parse_buffer[i];
	  messages.push_back(NMEA0183::parse_sentence(line));

	} else if ((_parse_buffer[0] == 0xa0) && (_parse_buffer[1] == 0xa1)) {
	  std::vector<uint8_t> msg;
	  for (std::size_t i = 0; i < end; i++)
	    msg.push_back(_parse_buffer[i]);
	  messages.push_back(SkyTraqBin::parse_message(msg));

	} else if ((_parse_buffer[0] == 0xb5) && (_parse_buffer[1] = 0x62)) {
	  std::vector<uint8_t> msg;
	  for (std::size_t i = 0; i < end; i++)
	    msg.push_back(_parse_buffer[i]);
	  messages.push_back(UBX::parse_message(msg));
	}

	// Catch the harmless exceptions
      } catch (const NMEA0183::InvalidSentence &e) {
      } catch (const GNSS::InvalidMessage &e) {
      } catch (const GNSS::InsufficientData &e) {

      } catch (const std::exception &e) {
	exp = std::current_exception();
      }

      // Remove this packet from the parse buffer
      for (std::size_t i = 0; i < end; i++)
	_parse_buffer.pop_front();

      if (exp != nullptr)
	std::rethrow_exception(exp);
    } while (!_parse_buffer.empty());

    return messages;
  }


  Interface::Interface(std::iostream&& f, Listener::ptr l) :
    _file(f), _listener(l), _response_pending(false)
  {}

  void Interface::_send_from_queue(void) {
    if (_output_queue.empty())
      return;

    auto msg = _output_queue.front();
    std::for_each(msg.cbegin(), msg.cend(), [&](const uint8_t& b) { _file << b; });
    _file.flush();

    _output_queue.pop();
    if (_output_queue.empty())
      _response_pending = false;
  }

  void Interface::read(void) {
    std::vector<uint8_t> buffer(16);
    _file.read(reinterpret_cast<char*>(buffer.data()), buffer.size());
    auto len = _file.gcount();

    if (len == 0) {
      if (_is_chrdev) {
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	return;
      } else {
	throw EndOfFile();
      }
    }

    _parser.add_bytes(buffer);

    auto messages = _parser.parse_messages();

    for (auto msg : messages) {

#define FIRE_IF(class, method) if (msg->isa<class>()) \
	_listener->method(this, *(msg->cast_as<class>()));

      try {
	msg->cast_as<NMEA0183::Sentence>();	// Will throw std::bad_cast if not possible

	FIRE_IF(NMEA0183::GGA, GGA)
	else FIRE_IF(NMEA0183::GLL, GLL)
	else FIRE_IF(NMEA0183::GSA, GSA)
	else FIRE_IF(NMEA0183::GSV, GSV)
	else FIRE_IF(NMEA0183::RMC, RMC)
	else FIRE_IF(NMEA0183::VTG, VTG)
	else FIRE_IF(NMEA0183::ZDA, ZDA);

      } catch (std::bad_cast &e) {
      }

      if (typeid(_listener) == typeid(Listener_SkyTraq)) {
	Listener_SkyTraq *l_s = dynamic_cast<Listener_SkyTraq*>(_listener.get());
#undef FIRE_IF
#define FIRE_IF(class, method) if (msg->isa<class>()) \
	l_s->method(this, *(msg->cast_as<class>()));

	try {
	  msg->cast_as<NMEA0183::Sentence>();	// Will throw std::bad_cast if not possible
	  FIRE_IF(NMEA0183::Skytraq::PPS, Skytraq_PPS)
	  else FIRE_IF(NMEA0183::Skytraq::Sensors, Skytraq_Sensors);
	} catch (std::bad_cast &e) {
	}

	try {
	  auto m = msg->cast_as<SkyTraqBin::Output_message>();	// Will throw a std::bad_cast exception if not possible

	  if (msg->isa<SkyTraqBin::Ack>()) {
	    auto ack = msg->cast_as<SkyTraqBin::Ack>();
	    uint16_t id = ack->ack_id();
	    if (ack->has_subid())
	      id = (id << 8) | ack->ack_subid();

	    bool response_type = false;
	    uint16_t rid;
	    try {
	      auto *wr = msg->cast_as<SkyTraqBin::with_response>();
	      rid = wr->response_id();
	      if (wr->has_response_subid())
		rid = (rid << 8) | wr->response_subid();
	      response_type = true;
	    } catch (std::bad_cast &e) {
	    }

	    if (_response_handlers_skytraq.count(id) > 0) {
	      // Call the response handler with a null message
	      _response_handlers_skytraq[id](true, nullptr);

	      // If there is a response message type, move the handler to its id
	      if (response_type)
		_response_handlers_skytraq[rid] = _response_handlers_skytraq[id];

	      _response_handlers_skytraq.erase(id);
	    }

	    // If there is no additional "response" message incoming, it's okay to send another message
	    if (!response_type)
	      _send_from_queue();

	  } else if (msg->isa<SkyTraqBin::Nack>()) {
	    // There's no additional "response" message, send another message
	    _send_from_queue();

	    auto nack = msg->cast_as<SkyTraqBin::Nack>();
	    uint16_t id = nack->nack_id();
	    if (nack->has_subid())
	      id = (id << 8) | nack->nack_subid();

	    if (_response_handlers_skytraq.count(id) > 0) {
	      // Call the response handler with a null message
	      _response_handlers_skytraq[id](false, nullptr);
	      _response_handlers_skytraq.erase(id);
	    }

	  } else FIRE_IF(SkyTraqBin::Nav_data_msg, Navigation_data)
	  else FIRE_IF(SkyTraqBin::Sensor_data, Sensor_data)
	  else FIRE_IF(SkyTraqBin::Measurement_time, Measurement_time)
	  else FIRE_IF(SkyTraqBin::Raw_measurements, Raw_measurements)
	  else FIRE_IF(SkyTraqBin::SV_channel_status, SV_channel_status)
	  else FIRE_IF(SkyTraqBin::Rcv_state, Rcv_state)
	  else FIRE_IF(SkyTraqBin::GPS_subframe_data, GPS_subframe_data)
	  else {
	    // Assume this is a "response" ouput message, send another message from the queue
	    _send_from_queue();

	    uint16_t id = m->message_id();
	    try {
	      auto m_with_subid = m->cast_as<SkyTraqBin::with_subid>();
	      id = (id << 8) | m_with_subid->message_subid();
	    } catch (std::bad_cast &e) {
	    }

	    if (_response_handlers_skytraq.count(id) > 0) {
	      // Call the response handler with the message
	      _response_handlers_skytraq[id](true, m);
	      _response_handlers_skytraq.erase(id);
	    }
	  }

	} catch (std::bad_cast &e) {
	}
      }

#undef FIRE_IF
    }
  }

  void Interface::set_is_chrdev(bool c) {
    _is_chrdev = c;
  }

  void Interface::send(SkyTraqBin::Input_message::ptr msg) {
    if (!_is_chrdev)
      throw NotSendable();

    std::vector<uint8_t> buffer(msg->message_length());
    msg->to_buf(buffer);

    if (!_response_pending) {
      std::for_each(buffer.cbegin(), buffer.cend(), [&](const uint8_t& b) { _file << b; });
      _file.flush();
      _response_pending = true;
    } else
      _output_queue.push(buffer);
  }

  void Interface::send(SkyTraqBin::Input_message::ptr msg, ResponseHandler_Skytraq rh) {
    send(msg);
    uint16_t id = msg->message_id();
    try {
      auto msg_with_subid = msg->cast_as<SkyTraqBin::with_subid>();
      id = (id << 8) | msg_with_subid->message_subid();
    } catch (std::bad_cast &e) {
    }
    _response_handlers_skytraq[id] = rh;
  }

  void Interface::send(UBX::Input_message::ptr msg) {
    if (!_is_chrdev)
      throw NotSendable();

    std::vector<uint8_t> buffer(msg->message_length());
    msg->to_buf(buffer);

    if (!_response_pending) {
      std::for_each(buffer.cbegin(), buffer.cend(), [&](const uint8_t& b) { _file << b; });
      _file.flush();
      _response_pending = true;
    } else
      _output_queue.push(buffer);
  }

  void Interface::send(UBX::Input_message::ptr msg, ResponseHandler_Ublox rh) {
    send(msg);
    uint16_t clsid = (static_cast<uint16_t>(msg->cls()) << 8) | msg->id();
    _response_handlers_ublox[clsid] = rh;
  }


}; // namespace GNSS
