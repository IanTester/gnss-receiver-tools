/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include "SkyTraqBin.hh"
#include "BE.hh"

/*
  Sources:
  https://store-lgdi92x.mybigcommerce.com/content/AN0028_1.4.31.pdf	(Binary messages of Skytraq Venus 8)
  https://store-lgdi92x.mybigcommerce.com/content/AN0024_v07.pdf	(Raw measurement binary messages of Skytraq 6 & 8)
  https://store-lgdi92x.mybigcommerce.com/content/SUP800F_v0.6.pdf	(Skytraq SUP800F datasheet)
  https://store-lgdi92x.mybigcommerce.com/content/AN0008_v1.4.17.pdf    (Datalogging extension for Venus 8)
*/

#define GETTER(type, class, name, field) const type class::name(void) const { return field; }

#define GETTER_SETTER(type, class, name, field) const type class::name(void) const { return field; } \
  void class::set_##name(type val) { field = val; }

#define SETTER_BOOL(class, name, field) void class::set_##name(bool val) { field = val; } \
  void class::unset_##name(void) { field = false; }

#define GETTER_RAW(type, class, name, field) const type class::name##_raw(void) const { return field; }

#define GETTER_SETTER_RAW(type, class, name, field) const type class::name##_raw(void) const { return field; } \
  void class::set_##name##_raw(type val) { field = val; }

#define GETTER_MOD(type, class, name, code) const type class::name(void) const { return code; }

#define GETTER_SETTER_MOD(type, class, name, field, code_get, code_set) const type class::name(void) const { return code_get; } \
  void class::set_##name(type val) { field = code_set; }

#define GETTER_ITERATORS(ctype, class, name, cont) ctype::iterator class::name##_begin(void) { return cont.begin(); } \
  ctype::iterator class::name##_end(void) { return cont.end(); }\
  ctype::reverse_iterator class::name##_rbegin(void) { return cont.rbegin(); }\
  ctype::reverse_iterator class::name##_rend(void) { return cont.rend(); }\
  ctype::const_iterator class::name##_cbegin(void) const { return cont.cbegin(); }\
  ctype::const_iterator class::name##_cend(void) const { return cont.cend(); }\
  ctype::const_reverse_iterator class::name##_crbegin(void) const { return cont.crbegin(); }\
  ctype::const_reverse_iterator class::name##_crend(void) const { return cont.crend(); }

#define RESPONSE1(class, id) uint8_t class::response_id(void) const { return id; } \
  bool class::has_response_subid(void) const { return false; }		\
  uint8_t class::response_subid(void) const { return 0; }

#define RESPONSE2(class, id, subid) uint8_t class::response_id(void) const { return id; } \
  bool class::has_response_subid(void) const { return true; }		\
  uint8_t class::response_subid(void) const { return subid; }


namespace SkyTraqBin {

  Restart_sys::Restart_sys(StartMode mode,
			   uint16_t y, uint8_t m, uint8_t d,
			   uint8_t hr, uint8_t min, uint8_t sec,
			   int16_t lat, int16_t lon, int16_t alt) :
    Input_message(0x01),
    _start_mode(mode),
    _utc_time(greg::date(y, m, d), ptime::time_duration(hr, min, sec)),
    _lattitude(lat), _longitude(lon), _altitude(alt)
  {}

  Restart_sys::Restart_sys(StartMode mode,
			   uint16_t y, uint8_t m, uint8_t d,
			   uint8_t hr, uint8_t min, uint8_t sec,
			   double lat, double lon, double alt) :
    Input_message(0x01),
    _start_mode(mode),
    _utc_time(greg::date(y, m, d), ptime::hours(hr) + ptime::minutes(min) + ptime::seconds(sec)),
    _lattitude(floor(0.5 + lat * 100)), _longitude(floor(0.5 + lon * 100)), _altitude(floor(0.5 + alt))
  {}

  GETTER_SETTER(StartMode, Restart_sys, start_mode, _start_mode);
  GETTER(ptime::ptime, Restart_sys, UTC_time, _utc_time);

  GETTER_SETTER_RAW(int16_t, Restart_sys, lattitude, _lattitude);
  GETTER_SETTER_MOD(double, Restart_sys, lattitude, _lattitude, _lattitude * 0.01, val * 100);

  GETTER_SETTER_RAW(int16_t, Restart_sys, longitude, _longitude);
  GETTER_SETTER_MOD(double, Restart_sys, longitude, _longitude, _longitude * 0.01, val * 100);

  GETTER_SETTER(int16_t, Restart_sys, altitude, _altitude);

  void Restart_sys::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_start_mode));

    greg::date date = _utc_time.date();
    append_be(buffer, static_cast<uint16_t>(date.year()));
    append_be(buffer, static_cast<uint8_t>(date.month()));
    append_be(buffer, static_cast<uint8_t>(date.day()));

    ptime::time_duration time = _utc_time.time_of_day();
    append_be(buffer, static_cast<uint8_t>(time.hours()));
    append_be(buffer, static_cast<uint8_t>(time.minutes()));
    append_be(buffer, static_cast<uint8_t>(time.seconds()));

    append_be(buffer, _lattitude);
    append_be(buffer, _longitude);
    append_be(buffer, _altitude);
  }


  Q_sw_ver::Q_sw_ver(SwType type) :
    Input_message(0x02),
    _sw_type(type)
  {}

  GETTER(Payload_length, Q_sw_ver, body_length, 1);

  void Q_sw_ver::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_sw_type));
  }

  RESPONSE1(Q_sw_ver, 0x80);

  GETTER_SETTER(SwType, Q_sw_ver, software_type, _sw_type);


  Q_sw_CRC::Q_sw_CRC(SwType type) :
    Input_message(0x03),
    _sw_type(type)
  {}

  GETTER(Payload_length, Q_sw_CRC, body_length, 1);

  void Q_sw_CRC::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_sw_type));
  }

  RESPONSE1(Q_sw_CRC, 0x81);

  GETTER_SETTER(SwType, Q_sw_CRC, software_type, _sw_type);


  Set_factory_defaults::Set_factory_defaults(bool r) :
    Input_message(0x04),
    _reset(r)
  {}

  GETTER(Payload_length, Set_factory_defaults, body_length, 1);

  void Set_factory_defaults::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_reset));
  }

  GETTER(bool, Set_factory_defaults, reset, _reset);
  SETTER_BOOL(Set_factory_defaults, reset, _reset);


  Config_serial_port::Config_serial_port(uint8_t cp, BaudRate br, UpdateType ut) :
    Input_message(0x05),
    _com_port(cp), _baud_rate(br), _update_type(ut)
  {}

  GETTER(Payload_length, Config_serial_port, body_length, 3);

  void Config_serial_port::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _com_port);
    append_be(buffer, static_cast<uint8_t>(_baud_rate));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint8_t, Config_serial_port, com_port, _com_port);
  GETTER_SETTER(BaudRate, Config_serial_port, baud_rate, _baud_rate);
  GETTER_SETTER(UpdateType, Config_serial_port, update_type, _update_type);


  Config_NMEA_msg::Config_NMEA_msg(uint8_t gga, uint8_t gsa, uint8_t gsv, uint8_t gll,
				   uint8_t rmc, uint8_t vtg, uint8_t zda, UpdateType ut) :
    Input_message(0x08),
    _gga_int(gga), _gsa_int(gsa), _gsv_int(gsv), _gll_int(gll),
    _rmc_int(rmc), _vtg_int(vtg), _zda_int(zda), _update_type(ut)
  {}

  GETTER(Payload_length, Config_NMEA_msg, body_length, 8);

  void Config_NMEA_msg::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _gga_int);
    append_be(buffer, _gsa_int);
    append_be(buffer, _gsv_int);
    append_be(buffer, _gll_int);
    append_be(buffer, _rmc_int);
    append_be(buffer, _vtg_int);
    append_be(buffer, _zda_int);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint8_t, Config_NMEA_msg, GGA_interval, _gga_int);
  GETTER_SETTER(uint8_t, Config_NMEA_msg, GSA_interval, _gsa_int);
  GETTER_SETTER(uint8_t, Config_NMEA_msg, GSV_interval, _gsv_int);
  GETTER_SETTER(uint8_t, Config_NMEA_msg, GLL_interval, _gll_int);
  GETTER_SETTER(uint8_t, Config_NMEA_msg, RMC_interval, _rmc_int);
  GETTER_SETTER(uint8_t, Config_NMEA_msg, VTG_interval, _vtg_int);
  GETTER_SETTER(uint8_t, Config_NMEA_msg, ZDA_interval, _zda_int);
  GETTER_SETTER(UpdateType, Config_NMEA_msg, update_type, _update_type);


  Config_msg_type::Config_msg_type(MessageType mt, UpdateType ut) :
    Input_message(0x09),
    _msg_type(mt),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_msg_type, body_length, 2);

  void Config_msg_type::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_msg_type));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(MessageType, Config_msg_type, message_type, _msg_type);
  GETTER_SETTER(UpdateType, Config_msg_type, update_type, _update_type);


  Sw_img_download::Sw_img_download(BaudRate br, BufferUsed bu) :
    Input_message(0x0b),
    _baud_rate(br),
    _flash_type(FlashType::Auto), _flash_id(0),
    _buffer_used(bu)
  {}

  Sw_img_download::Sw_img_download(BaudRate br, FlashType ft, uint16_t fid, BufferUsed bu) :
    Input_message(0x0b),
    _baud_rate(br),
    _flash_type(ft), _flash_id(fid),
    _buffer_used(bu)
  {}

  GETTER(Payload_length, Sw_img_download, body_length, 5);

  void Sw_img_download::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_baud_rate));
    append_be(buffer, static_cast<uint8_t>(_flash_type));
    append_be(buffer, _flash_id);
    append_be(buffer, static_cast<uint8_t>(_buffer_used));
  }

  GETTER_SETTER(BaudRate, Sw_img_download, baud_rate, _baud_rate);
  GETTER_SETTER(FlashType, Sw_img_download, flash_type, _flash_type);
  GETTER_SETTER(BufferUsed, Sw_img_download, buffer_used, _buffer_used);


  Config_sys_power_mode::Config_sys_power_mode(PowerMode pm, UpdateType ut) :
    Input_message(0x0c),
    _power_mode(pm),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_sys_power_mode, body_length, 2);

  void Config_sys_power_mode::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_power_mode));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(PowerMode, Config_sys_power_mode, power_mode, _power_mode);
  GETTER_SETTER(UpdateType, Config_sys_power_mode, update_type, _update_type);


  Config_sys_pos_rate::Config_sys_pos_rate(uint8_t r, UpdateType ut) :
    Input_message(0x0e),
    _rate(r),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_sys_pos_rate, body_length, 2);

  void Config_sys_pos_rate::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _rate);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint8_t, Config_sys_pos_rate, rate, _rate);
  GETTER_SETTER(UpdateType, Config_sys_pos_rate, update_type, _update_type);

  Q_pos_update_rate::Q_pos_update_rate(void) :
    Input_message(0x10)
  {}

  RESPONSE1(Q_pos_update_rate, 0x86);


  Config_nav_data_msg_interval::Config_nav_data_msg_interval(uint8_t i, UpdateType ut) :
    Input_message(0x11),
    _interval(i),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_nav_data_msg_interval, body_length, 2);

  void Config_nav_data_msg_interval::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _interval);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint8_t, Config_nav_data_msg_interval, interval, _interval);
  GETTER_SETTER(UpdateType, Config_nav_data_msg_interval, update_type, _update_type);


  Get_almanac::Get_almanac(uint8_t sv) :
    Input_message(0x11),
    _sv_num(sv)
  {}

  GETTER(Payload_length, Get_almanac, body_length, 1);

  void Get_almanac::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _sv_num);
  }

  GETTER_SETTER(uint8_t, Get_almanac, SV_num, _sv_num);
  RESPONSE1(Get_almanac, 0x87);


  Config_bin_measurement_output_rates::Config_bin_measurement_output_rates(OutputRate o, bool mt, bool rm, bool svch, bool rcv, bool sub, UpdateType ut) :
    Input_message(0x12),
    _output_rate(o),
    _meas_time(mt), _raw_meas(rm), _sv_ch_status(svch), _rcv_state(rcv), _subframe(sub),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_bin_measurement_output_rates, body_length, 7);

  void Config_bin_measurement_output_rates::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_output_rate));
    append_be(buffer, _meas_time);
    append_be(buffer, _raw_meas);
    append_be(buffer, _sv_ch_status);
    append_be(buffer, _rcv_state);
    append_be(buffer, _subframe);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(OutputRate, Config_bin_measurement_output_rates, output_rate, _output_rate);
  GETTER(bool, Config_bin_measurement_output_rates, meas_time, _meas_time);
  SETTER_BOOL(Config_bin_measurement_output_rates, meas_time, _meas_time);

  GETTER(bool, Config_bin_measurement_output_rates, raw_meas, _raw_meas);
  SETTER_BOOL(Config_bin_measurement_output_rates, raw_meas, _raw_meas);

  GETTER(bool, Config_bin_measurement_output_rates, SV_CH_status, _sv_ch_status);
  SETTER_BOOL(Config_bin_measurement_output_rates, SV_CH_status, _sv_ch_status);

  GETTER(bool, Config_bin_measurement_output_rates, RCV_state, _rcv_state);
  SETTER_BOOL(Config_bin_measurement_output_rates, RCV_state, _rcv_state);

  GETTER(bool, Config_bin_measurement_output_rates, subframe, _subframe);
  SETTER_BOOL(Config_bin_measurement_output_rates, subframe, _subframe);

  GETTER_SETTER(UpdateType, Config_bin_measurement_output_rates, update_type, _update_type);


  Q_power_mode::Q_power_mode(void) :
    Input_message(0x15)
  {}

  RESPONSE1(Q_power_mode, 0xB9);


  Q_log_status::Q_log_status(void) :
    Input_message(0x17)
  {}

  RESPONSE1(Q_log_status, 0x94);


  Config_logging::Config_logging(uint32_t max_t, uint32_t min_t, uint32_t max_d, uint32_t min_d,
		 uint32_t max_s, uint32_t min_s, bool dl) :
    Input_message(0x18),
    _max_time(max_t), _min_time(min_t),
    _max_dist(max_d), _min_dist(min_d),
    _max_speed(max_s), _min_speed(min_s),
    _datalog(dl)
  {}

  GETTER(Payload_length, Config_logging, body_length, 26);

  void Config_logging::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _max_time);
    append_be(buffer, _min_time);
    append_be(buffer, _max_dist);
    append_be(buffer, _min_dist);
    append_be(buffer, _max_speed);
    append_be(buffer, _min_speed);
    append_be(buffer, static_cast<uint8_t>(_datalog));
    append_be(buffer, static_cast<uint8_t>(0)); // reseverved byte
  }

  GETTER_SETTER(uint32_t, Config_logging, max_time, _max_time);
  GETTER_SETTER(uint32_t, Config_logging, min_time, _min_time);
  GETTER_SETTER(uint32_t, Config_logging, max_distance, _max_dist);
  GETTER_SETTER(uint32_t, Config_logging, min_distance, _min_dist);
  GETTER_SETTER(uint32_t, Config_logging, max_speed, _max_speed);
  GETTER_SETTER(uint32_t, Config_logging, min_speed, _min_speed);

  GETTER(bool, Config_logging, datalog, _datalog);
  SETTER_BOOL(Config_logging, datalog, _datalog);


  Clear_log::Clear_log(void) :
    Input_message(0x19)
  {}


  Read_log::Read_log(uint16_t ss, uint16_t num) :
    Input_message(0x1d),
    _start_sector(ss), _num_sectors(num)
  {}

  GETTER(Payload_length, Read_log, body_length, 24);

  void Read_log::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _start_sector);
    append_be(buffer, _num_sectors);
  }

  GETTER_SETTER(uint16_t, Read_log, start_sector, _start_sector);
  GETTER_SETTER(uint16_t, Read_log, num_sectors, _num_sectors);


  Config_bin_measurement_data_output::Config_bin_measurement_data_output(OutputRate o, bool mt, bool rm,
									 bool svch, bool rcv, bool s_gps,
									 bool s_glo, bool s_gal, bool s_bei,
									 UpdateType ut) :
    Input_message(0x1e),
    _output_rate(o),
    _meas_time(mt), _raw_meas(rm), _sv_ch_status(svch), _rcv_state(rcv),
    _sub_gps(s_gps), _sub_glonass(s_glo), _sub_galileo(s_gal), _sub_beidou2(s_bei),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_bin_measurement_data_output, body_length, 7);

  void Config_bin_measurement_data_output::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_output_rate));
    append_be(buffer, _meas_time);
    append_be(buffer, _raw_meas);
    append_be(buffer, _sv_ch_status);
    append_be(buffer, _rcv_state);
    append_be(buffer, static_cast<uint8_t>(_sub_gps) | (static_cast<uint8_t>(_sub_glonass) << 1)
	      | (static_cast<uint8_t>(_sub_galileo) << 2) | (static_cast<uint8_t>(_sub_beidou2) << 3));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(OutputRate, Config_bin_measurement_data_output, output_rate, _output_rate);
  GETTER(bool, Config_bin_measurement_data_output, meas_time, _meas_time);
  SETTER_BOOL(Config_bin_measurement_data_output, meas_time, _meas_time);

  GETTER(bool, Config_bin_measurement_data_output, raw_meas, _raw_meas);
  SETTER_BOOL(Config_bin_measurement_data_output, raw_meas, _raw_meas);

  GETTER(bool, Config_bin_measurement_data_output, SV_CH_status, _sv_ch_status);
  SETTER_BOOL(Config_bin_measurement_data_output, SV_CH_status, _sv_ch_status);

  GETTER(bool, Config_bin_measurement_data_output, RCV_state, _rcv_state);
  SETTER_BOOL(Config_bin_measurement_data_output, RCV_state, _rcv_state);

  GETTER(bool, Config_bin_measurement_data_output, subframe_GPS, _sub_gps);
  SETTER_BOOL(Config_bin_measurement_data_output, subframe_GPS, _sub_gps);

  GETTER(bool, Config_bin_measurement_data_output, subframe_GLONASS, _sub_glonass);
  SETTER_BOOL(Config_bin_measurement_data_output, subframe_GLONASS, _sub_glonass);

  GETTER(bool, Config_bin_measurement_data_output, subframe_Galileo, _sub_galileo);
  SETTER_BOOL(Config_bin_measurement_data_output, subframe_Galileo, _sub_galileo);

  GETTER(bool, Config_bin_measurement_data_output, subframe_Beidou2, _sub_beidou2);
  SETTER_BOOL(Config_bin_measurement_data_output, subframe_Beidou2, _sub_beidou2);

  GETTER_SETTER(UpdateType, Config_bin_measurement_data_output, update_type, _update_type);


  Q_bin_messurement_data_output_status::Q_bin_messurement_data_output_status(void) :
    Input_message(0x1f)
  {}

  RESPONSE1(Q_bin_messurement_data_output_status, 0x89);


  Config_datum::Config_datum(uint16_t di, uint8_t ei,
			     int16_t dx, int16_t dy, int16_t dz,
			     uint32_t sma, uint32_t inf, UpdateType ut) :
    Input_message(0x29),
    _datum_index(di), _ellip_index(ei),
    _delta_x(dx), _delta_y(dy), _delta_z(dz),
    _semi_major_axis(sma), _inv_flattening(inf),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_datum, body_length, 18);

  void Config_datum::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _datum_index);
    append_be(buffer, _ellip_index);
    append_be(buffer, _delta_x);
    append_be(buffer, _delta_y);
    append_be(buffer, _delta_z);
    append_be(buffer, _semi_major_axis);
    append_be(buffer, _inv_flattening);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint16_t, Config_datum, datum_index, _datum_index);
  GETTER_SETTER(uint8_t, Config_datum, ellipsoid_index, _ellip_index);
  GETTER_SETTER(int16_t, Config_datum, delta_X, _delta_x);
  GETTER_SETTER(int16_t, Config_datum, delta_Y, _delta_y);
  GETTER_SETTER(int16_t, Config_datum, delta_Z, _delta_z);
  GETTER_SETTER(uint32_t, Config_datum, semi_major_axis, _semi_major_axis);
  GETTER_SETTER(uint32_t, Config_datum, inv_flattening, _inv_flattening);
  GETTER_SETTER(UpdateType, Config_datum, update_type, _update_type);


  Config_DOP_mask::Config_DOP_mask(DOPmode m, double p, double h, double g, UpdateType ut) :
    Input_message(0x2A),
    _dop_mode(m),
    _pdop(p * 10), _hdop(h * 10), _gdop(g * 10),
    _update_type(ut)
  {}

  Config_DOP_mask::Config_DOP_mask(DOPmode m, uint16_t p, uint16_t h, uint16_t g, UpdateType ut) :
    Input_message(0x2A),
    _dop_mode(m),
    _pdop(p), _hdop(h), _gdop(g),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_DOP_mask, body_length, 8);

  void Config_DOP_mask::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_dop_mode));
    append_be(buffer, _pdop);
    append_be(buffer, _hdop);
    append_be(buffer, _gdop);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(DOPmode, Config_DOP_mask, DOP_mode, _dop_mode);

  GETTER_SETTER_MOD(double, Config_DOP_mask, PDOP, _pdop, _pdop * 0.1, val * 10);
  GETTER_SETTER_RAW(uint16_t, Config_DOP_mask, PDOP, _pdop);

  GETTER_SETTER_MOD(double, Config_DOP_mask, HDOP, _hdop, _hdop * 0.1, val * 10);
  GETTER_SETTER_RAW(uint16_t, Config_DOP_mask, HDOP, _hdop);

  GETTER_SETTER_MOD(double, Config_DOP_mask, GDOP, _gdop, _gdop * 0.1, val * 10);
  GETTER_SETTER_RAW(uint16_t, Config_DOP_mask, GDOP, _gdop);

  GETTER_SETTER(UpdateType, Config_DOP_mask, update_type, _update_type);


  Config_elevation_CNR_mask::Config_elevation_CNR_mask(ElevationCNRmode ms, uint8_t em, uint8_t cm, UpdateType ut) :
    Input_message(0x2B),
    _mode_select(ms),
    _el_mask(em), _cnr_mask(cm),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_elevation_CNR_mask, body_length, 8);

  void Config_elevation_CNR_mask::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_mode_select));
    append_be(buffer, _el_mask);
    append_be(buffer, _cnr_mask);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(ElevationCNRmode, Config_elevation_CNR_mask, mode_select, _mode_select);
  GETTER_SETTER(uint8_t, Config_elevation_CNR_mask, elevation_mask, _el_mask);
  GETTER_SETTER(uint8_t, Config_elevation_CNR_mask, CNR_mask, _cnr_mask);
  GETTER_SETTER(UpdateType, Config_elevation_CNR_mask, update_type, _update_type);


  Q_datum::Q_datum(void) :
    Input_message(0x2D)
  {}

  RESPONSE1(Q_datum, 0xAE);


  Q_DOP_mask::Q_DOP_mask(void) :
    Input_message(0x2E)
  {}

  RESPONSE1(Q_DOP_mask, 0xAF);


  Q_elevation_CNR_mask::Q_elevation_CNR_mask(void) :
    Input_message(0x2F)
  {}

  RESPONSE1(Q_elevation_CNR_mask, 0xB0);


  Get_GPS_ephemeris::Get_GPS_ephemeris(uint8_t sv) :
    Input_message(0x30),
    _sv_num(sv)
  {}

  GETTER(Payload_length, Get_GPS_ephemeris, body_length, 1);

  void Get_GPS_ephemeris::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _sv_num);
  }

  RESPONSE1(Get_GPS_ephemeris, 0xB1);

  GETTER_SETTER(uint8_t, Get_GPS_ephemeris, SV_number, _sv_num);


  Config_pos_pinning::Config_pos_pinning(DefaultOrEnable p, UpdateType ut) :
    Input_message(0x39),
    _pinning(p), _update_type(ut)
  {}

  GETTER(Payload_length, Config_pos_pinning, body_length, 2);

  void Config_pos_pinning::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_pinning));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(DefaultOrEnable, Config_pos_pinning, pinning, _pinning);
  GETTER_SETTER(UpdateType, Config_pos_pinning, update_type, _update_type);


  Q_pos_pinning::Q_pos_pinning(void) :
    Input_message(0x3A)
  {}

  RESPONSE1(Q_pos_pinning, 0xB4);


  Config_pos_pinning_params::Config_pos_pinning_params(uint16_t ps, uint16_t pc,
						       uint16_t us, uint16_t uc, uint16_t ud, UpdateType ut) :
    Input_message(0x3B),
    _pin_speed(ps), _pin_count(pc),
    _unpin_speed(us), _unpin_count(uc), _unpin_dist(ud),
      _update_type(ut)
  {}

  GETTER(Payload_length, Config_pos_pinning_params, body_length, 1);

  void Config_pos_pinning_params::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _pin_speed);
    append_be(buffer, _pin_count);
    append_be(buffer, _unpin_speed);
    append_be(buffer, _unpin_count);
    append_be(buffer, _unpin_dist);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint16_t, Config_pos_pinning_params, pinning_speed, _pin_speed);
  GETTER_SETTER(uint16_t, Config_pos_pinning_params, pinning_count, _pin_count);
  GETTER_SETTER(uint16_t, Config_pos_pinning_params, unpinning_speed, _unpin_speed);
  GETTER_SETTER(uint16_t, Config_pos_pinning_params, unpinning_count, _unpin_count);
  GETTER_SETTER(uint16_t, Config_pos_pinning_params, unpinning_distance, _unpin_dist);
  GETTER_SETTER(UpdateType, Config_pos_pinning_params, update_type, _update_type);


  Set_GPS_ephemeris::Set_GPS_ephemeris(uint16_t sv, const std::vector<uint8_t>& sf1,
				       std::vector<uint8_t>& sf2, std::vector<uint8_t>& sf3) :
    Input_message(0x41),
    _sv_num(sv),
    _subframe1(224), _subframe2(224), _subframe3(224)
  {
    for (uint8_t i = 0; i < 28; i++)
      _subframe1.insert(sf1[i], i * 8);

    for (uint8_t i = 0; i < 28; i++)
      _subframe2.insert(sf2[i], i * 8);

    for (uint8_t i = 0; i < 28; i++)
      _subframe3.insert(sf3[i], i * 8);
  }

  GETTER(Payload_length, Set_GPS_ephemeris, body_length, 86);

  void Set_GPS_ephemeris::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _sv_num);
    int i;
    for (i = 0; i < 28; i++)
      append_be(buffer, _subframe1.extract<uint8_t>(i * 8));
    for (i = 0; i < 28; i++)
      append_be(buffer, _subframe2.extract<uint8_t>(i * 8));
    for (i = 0; i < 28; i++)
      append_be(buffer, _subframe3.extract<uint8_t>(i * 8));
  }

  GETTER_SETTER(uint16_t, Set_GPS_ephemeris, SV_number, _sv_num);

  GETTER(bitstream&, Set_GPS_ephemeris, subframe1, _subframe1);
  GETTER(bitstream&, Set_GPS_ephemeris, subframe2, _subframe2);
  GETTER(bitstream&, Set_GPS_ephemeris, subframe3, _subframe3);


  Q_1PPS_timing::Q_1PPS_timing(void) :
    Input_message(0x44)
  {}

  RESPONSE1(Q_1PPS_timing, 0xc2);


  GETTER(Payload_length, Config_1PPS_cable_delay, body_length, 5);

  void Config_1PPS_cable_delay::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _delay);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  Config_1PPS_cable_delay::Config_1PPS_cable_delay(int32_t d, UpdateType ut) :
    Input_message(0x45),
    _delay(d), _update_type(ut)
  {}

  Config_1PPS_cable_delay::Config_1PPS_cable_delay(double d, UpdateType ut) :
    Input_message(0x45),
    _delay(d * 1e+11), _update_type(ut)
  {}

  GETTER_SETTER_MOD(double, Config_1PPS_cable_delay, delay, _delay, _delay * 1.0e-11, val * 1e+11);
  GETTER_SETTER_RAW(int32_t, Config_1PPS_cable_delay, delay, _delay);
  GETTER_SETTER(UpdateType, Config_1PPS_cable_delay, update_type, _update_type);


  Q_1PPS_cable_delay::Q_1PPS_cable_delay(void) :
    Input_message(0x46)
  {}

  RESPONSE1(Q_1PPS_cable_delay, 0xBB);


  Config_NMEA_talker_ID::Config_NMEA_talker_ID(TalkerID id, UpdateType ut) :
    Input_message(0x4b),
    _talker_id(id),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_NMEA_talker_ID, body_length, 2);

  void Config_NMEA_talker_ID::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_talker_id));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(TalkerID, Config_NMEA_talker_ID, talker_id, _talker_id);
  GETTER_SETTER(UpdateType, Config_NMEA_talker_ID, update_type, _update_type);


  Q_NMEA_talker_ID::Q_NMEA_talker_ID(void) :
    Input_message(0x4f)
  {}

  RESPONSE1(Q_NMEA_talker_ID, 0x93);


  Config_1PPS_timing::Config_1PPS_timing(SkyTraq::TimingMode tm, uint32_t sl, uint32_t sd, double lat, double lon, float alt, UpdateType ut) :
    Input_message(0x54),
    _timing_mode(tm),
    _survey_len(sl),
    _std_dev(sd),
    _lat(lat), _lon(lon), _alt(alt),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_1PPS_timing, body_length, 30);

  void Config_1PPS_timing::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_timing_mode));
    append_be(buffer, _survey_len);
    append_be(buffer, _std_dev);
    append_be(buffer, _lat);
    append_be(buffer, _lon);
    append_be(buffer, _alt);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(SkyTraq::TimingMode, Config_1PPS_timing, timing_mode, _timing_mode);
  GETTER_SETTER(uint32_t, Config_1PPS_timing, survey_length, _survey_len);
  GETTER_SETTER(uint32_t, Config_1PPS_timing, standard_deviation, _std_dev);
  GETTER_SETTER(double, Config_1PPS_timing, lattitude, _lat);
  GETTER_SETTER(double, Config_1PPS_timing, longitude, _lon);
  GETTER_SETTER(float, Config_1PPS_timing, altitude, _alt);
  GETTER_SETTER(UpdateType, Config_1PPS_timing, update_type, _update_type);


  Get_Glonass_ephemeris::Get_Glonass_ephemeris(uint8_t slot) :
    Input_message(0x5b),
    _slot_number(slot)
  {}

  GETTER(Payload_length, Get_Glonass_ephemeris, body_length, 1);

  void Get_Glonass_ephemeris::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _slot_number);
  }

  RESPONSE1(Get_Glonass_ephemeris, 0x90);

  GETTER_SETTER(uint8_t, Get_Glonass_ephemeris, slot_number, _slot_number);


  Set_Glonass_ephemeris::Set_Glonass_ephemeris(uint8_t slot, int8_t k, const std::vector<uint8_t>& s1,
					       const std::vector<uint8_t>& s2,
					       const std::vector<uint8_t>& s3,
					       const std::vector<uint8_t>& s4) :
    Input_message(0x5c),
    _slot_number(slot), _k_number(k),
    _string1(80), _string2(80), _string3(80), _string4(80)
  {
    for (uint8_t i = 0; i < 10; i++)
      _string1.insert(s1[i], i * 8);

    for (uint8_t i = 0; i < 10; i++)
      _string2.insert(s2[i], i * 8);

    for (uint8_t i = 0; i < 10; i++)
      _string3.insert(s3[i], i * 8);

    for (uint8_t i = 0; i < 10; i++)
      _string4.insert(s4[i], i * 8);
  }

  GETTER(Payload_length, Set_Glonass_ephemeris, body_length, 42);

  void Set_Glonass_ephemeris::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _slot_number);
    append_be(buffer, _k_number);
    int i;
    for (i = 0; i < 10; i++)
      append_be(buffer, _string1.extract<uint8_t>(i * 8));
    for (i = 0; i < 10; i++)
      append_be(buffer, _string2.extract<uint8_t>(i * 8));
    for (i = 0; i < 10; i++)
      append_be(buffer, _string3.extract<uint8_t>(i * 8));
    for (i = 0; i < 10; i++)
      append_be(buffer, _string4.extract<uint8_t>(i * 8));
  }

  GETTER_SETTER(uint8_t, Set_Glonass_ephemeris, slot_number, _slot_number);
  GETTER_SETTER(int8_t, Set_Glonass_ephemeris, k_number, _k_number);

  GETTER(bitstream&, Set_Glonass_ephemeris, string1, _string1);
  GETTER(bitstream&, Set_Glonass_ephemeris, string2, _string2);
  GETTER(bitstream&, Set_Glonass_ephemeris, string3, _string3);
  GETTER(bitstream&, Set_Glonass_ephemeris, string4, _string4);


  /**************************
   * Messages with a sub-ID *
   **************************/


  Input_message_with_subid::Input_message_with_subid(uint8_t id, uint8_t subid) :
    Input_message(id),
    with_subid(subid)
  {}

  const Payload_length Input_message_with_subid::body_length(void) const {
    return 0;
  }

  void Input_message_with_subid::body_to_buf(std::vector<uint8_t>& payload) const {
  }

  const Payload_length Input_message_with_subid::message_length(void) const {
    return StartSeq_len + PayloadLength_len + MsgID_len + MsgSubID_len + body_length() + Checksum_len + EndSeq_len;
  }

  void Input_message_with_subid::to_buf(std::vector<uint8_t>& buffer) const {
    append_be<uint8_t>(buffer, 0xa0);
    append_be<uint8_t>(buffer, 0xa1);

    Payload_length payload_len = body_length() + MsgID_len + MsgSubID_len;
    append_be(buffer, payload_len);

    auto payload_start = buffer.size();
    append_be(buffer, _msg_id);
    append_be(buffer, _msg_subid);
    body_to_buf(buffer);

    append_be(buffer, checksum(buffer.cbegin() + payload_start, buffer.cend()));
    append_be<uint8_t>(buffer, 0x0d);
    append_be<uint8_t>(buffer, 0x0a);
  }


  Config_SBAS::Config_SBAS(bool en, EnableOrAuto r, uint8_t rm, bool c, uint8_t nc, bool w, bool e, bool m, bool a, UpdateType ut) :
    Input_message_with_subid(0x62, 0x01),
    _enable(en),
    _ranging(r), _ranging_ura_mask(rm),
    _correction(c), _num_channels(nc),
    _waas(w), _egnos(e), _msas(m), _all_sbas(a),
    _update_type(ut)
  {}

  Config_SBAS::Config_SBAS(bool en, EnableOrAuto r, uint8_t rm, bool c, uint8_t nc, bool w, bool e, bool m, UpdateType ut) :
    Input_message_with_subid(0x62, 0x01),
    _enable(en),
    _ranging(r), _ranging_ura_mask(rm),
    _correction(c), _num_channels(nc),
    _waas(w), _egnos(e), _msas(m), _all_sbas(false),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_SBAS, body_length, 8);

  void Config_SBAS::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_enable));
    append_be(buffer, static_cast<uint8_t>(_ranging));
    append_be(buffer, _ranging_ura_mask);
    append_be(buffer, static_cast<uint8_t>(_correction));
    append_be(buffer, _num_channels);
    append_be(buffer, static_cast<uint8_t>(_waas) | (static_cast<uint8_t>(_egnos) << 1) | (static_cast<uint8_t>(_msas) << 2) | (static_cast<uint8_t>(_msas) << 7));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER(bool, Config_SBAS, enabled, _enable);
  void Config_SBAS::enable(bool en) { _enable = en; }
  void Config_SBAS::disable(void) { _enable = false; }

  GETTER_SETTER(EnableOrAuto, Config_SBAS, ranging, _ranging);
  GETTER_SETTER(uint8_t, Config_SBAS, ranging_URA_mask, _ranging_ura_mask);

  GETTER(bool, Config_SBAS, correction, _correction);
  SETTER_BOOL(Config_SBAS, correction, _correction);

  GETTER_SETTER(uint8_t, Config_SBAS, num_channels, _num_channels);

  GETTER(bool, Config_SBAS, WAAS_enabled, _waas);
  void Config_SBAS::enable_WAAS(bool w) { _waas = w; }
  void Config_SBAS::disable_WAAS(void) { _waas = false; }

  GETTER(bool, Config_SBAS, EGNOS_enabled, _egnos);
  void Config_SBAS::enable_EGNOS(bool e) { _egnos = e; }
  void Config_SBAS::disable_EGNOS(void) { _egnos = false; }

  GETTER(bool, Config_SBAS, MSAS_enabled, _msas);
  void Config_SBAS::enable_MSAS(bool m) { _msas = m; }
  void Config_SBAS::disable_MSAS(void) { _msas = false; }

  GETTER(bool, Config_SBAS, All_SBAS_enabled, _all_sbas);
  void Config_SBAS::enable_All_SBAS(bool a) { _all_sbas = a; }
  void Config_SBAS::disable_All_SBAS(void) { _all_sbas = false; }

  GETTER_SETTER(UpdateType, Config_SBAS, update_type, _update_type);


  Q_SBAS_status::Q_SBAS_status(void) :
    Input_message_with_subid(0x62, 0x02)
  {}

  RESPONSE2(Q_SBAS_status, 0x62, 0x80);


  Config_QZSS::Config_QZSS(bool e, uint8_t nc, UpdateType ut) :
    Input_message_with_subid(0x62, 0x03),
    _enable(e), _num_channels(nc), _update_type(ut)
  {}

  GETTER(Payload_length, Config_QZSS, body_length, 3);

  void Config_QZSS::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_enable));
    append_be(buffer, _num_channels);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER(bool, Config_QZSS, enabled, _enable);
  void Config_QZSS::enable(bool en) { _enable = en; }
  void Config_QZSS::disable(void) { _enable = false; }

  GETTER_SETTER(uint8_t, Config_QZSS, num_channels, _num_channels);

  GETTER_SETTER(UpdateType, Config_QZSS, update_type, _update_type);


  Q_QZSS_status::Q_QZSS_status(void) :
    Input_message_with_subid(0x62, 0x04)
  {}

  RESPONSE2(Q_QZSS_status, 0x62, 0x81);


  Config_SAEE::Config_SAEE(DefaultOrEnable e, UpdateType ut) :
    Input_message_with_subid(0x63, 0x01),
    _enable(e), _update_type(ut)
  {}

  GETTER(Payload_length, Config_SAEE, body_length, 2);

  void Config_SAEE::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_enable));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(DefaultOrEnable, Config_SAEE, enable, _enable);
  GETTER_SETTER(UpdateType, Config_SAEE, update_type, _update_type);


  Q_SAEE_status::Q_SAEE_status(void) :
    Input_message_with_subid(0x63, 0x02)
  {}

  RESPONSE2(Q_SAEE_status, 0x63, 0x80);


  Q_GNSS_boot_status::Q_GNSS_boot_status(void) :
    Input_message_with_subid(0x64, 0x01)
  {}

  RESPONSE2(Q_GNSS_boot_status, 0x64, 0x80);


  Config_extended_NMEA_msg_interval::Config_extended_NMEA_msg_interval(uint8_t gga, uint8_t gsa, uint8_t gsv, uint8_t gll,
				    uint8_t rmc, uint8_t vtg, uint8_t zda, uint8_t gns,
				    uint8_t gbs, uint8_t grs, uint8_t dtm, uint8_t gst,
				    UpdateType ut) :
    Input_message_with_subid(0x64, 0x02),
    _gga(gga), _gsa(gsa), _gsv(gsv), _gll(gll),
    _rmc(rmc), _vtg(vtg), _zda(zda), _gns(gns),
    _gbs(gbs), _grs(grs), _dtm(dtm), _gst(gst),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_extended_NMEA_msg_interval, body_length, 13);

  void Config_extended_NMEA_msg_interval::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _gga);
    append_be(buffer, _gsa);
    append_be(buffer, _gsv);
    append_be(buffer, _gll);
    append_be(buffer, _rmc);
    append_be(buffer, _vtg);
    append_be(buffer, _zda);
    append_be(buffer, _gns);
    append_be(buffer, _gbs);
    append_be(buffer, _grs);
    append_be(buffer, _dtm);
    append_be(buffer, _gst);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GGA_interval, _gga);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GSA_interval, _gsa);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GSV_interval, _gsv);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GLL_interval, _gll);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, RMC_interval, _rmc);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, VTG_interval, _vtg);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, ZDA_interval, _zda);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GNS_interval, _gns);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GBS_interval, _gbs);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GRS_interval, _grs);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, DTM_interval, _dtm);
  GETTER_SETTER(uint8_t, Config_extended_NMEA_msg_interval, GST_interval, _gst);
  GETTER_SETTER(UpdateType, Config_extended_NMEA_msg_interval, update_type, _update_type);


  Q_extended_NMEA_msg_interval::Q_extended_NMEA_msg_interval(void) :
    Input_message_with_subid(0x64, 0x03)
  {}

  RESPONSE2(Q_extended_NMEA_msg_interval, 0x64, 0x81);


  Config_interference_detection::Config_interference_detection(bool e, UpdateType ut) :
    Input_message_with_subid(0x64, 0x06),
    _enable(e), _update_type(ut)
  {}

  GETTER(Payload_length, Config_interference_detection, body_length, 2);

  void Config_interference_detection::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_enable));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER(bool, Config_interference_detection, enabled, _enable);
  void Config_interference_detection::enable(bool en) { _enable = en; }
  void Config_interference_detection::disable(void) { _enable = false; }

  GETTER_SETTER(UpdateType, Config_interference_detection, update_type, _update_type);


  Q_interference_detection_status::Q_interference_detection_status(void) :
    Input_message_with_subid(0x64, 0x07)
  {}

  RESPONSE2(Q_interference_detection_status, 0x64, 0x83);


  Config_GPS_param_search_engine_num::Config_GPS_param_search_engine_num(ParameterSearchEngineMode pm, UpdateType ut) :
    Input_message_with_subid(0x64, 0x0a),
    _pse_mode(pm), _update_type(ut)
  {}

  GETTER(Payload_length, Config_GPS_param_search_engine_num, body_length, 2);

  void Config_GPS_param_search_engine_num::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_pse_mode));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(ParameterSearchEngineMode, Config_GPS_param_search_engine_num, pse_mode, _pse_mode);
  GETTER_SETTER(UpdateType, Config_GPS_param_search_engine_num, update_type, _update_type);


  Q_GPS_param_search_engine_num::Q_GPS_param_search_engine_num(void) :
    Input_message_with_subid(0x64, 0x0b)
  {}

  RESPONSE2(Q_GPS_param_search_engine_num, 0x64, 0x85);


  Config_GNSS_nav_mode::Config_GNSS_nav_mode(NavigationMode m, UpdateType ut) :
    Input_message_with_subid(0x64, 0x17),
    _mode(m), _update_type(ut)
  {}

  GETTER(Payload_length, Config_GNSS_nav_mode, body_length, 2);

  void Config_GNSS_nav_mode::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, static_cast<uint8_t>(_mode));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(NavigationMode, Config_GNSS_nav_mode, navigation_mode, _mode);
  GETTER_SETTER(UpdateType, Config_GNSS_nav_mode, update_type, _update_type);


  Q_GNSS_nav_mode::Q_GNSS_nav_mode(void) :
    Input_message_with_subid(0x64, 0x18)
  {}

  RESPONSE2(Q_GNSS_nav_mode, 0x64, 0x8B);


  Config_constellation_type::Config_constellation_type(bool gp, bool gl, bool ga, bool bd, UpdateType ut) :
    Input_message_with_subid(0x64, 0x19),
    _gps(gp), _glonass(gl), _galileo(ga), _beidou(bd), _update_type(ut)
  {}

  GETTER(Payload_length, Config_constellation_type, body_length, 3);

  void Config_constellation_type::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer,
	      static_cast<uint16_t>(_gps)
	      | (static_cast<uint16_t>(_glonass) << 1)
	      | (static_cast<uint8_t>(_galileo) << 2)
	      | (static_cast<uint8_t>(_beidou) << 3));
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER(bool, Config_constellation_type, GPS, _gps);
  SETTER_BOOL(Config_constellation_type, GPS, _gps);

  GETTER(bool, Config_constellation_type, GLONASS, _glonass);
  SETTER_BOOL(Config_constellation_type, GLONASS, _glonass);

  GETTER(bool, Config_constellation_type, Galileo, _galileo);
  SETTER_BOOL(Config_constellation_type, Galileo, _galileo);

  GETTER(bool, Config_constellation_type, Beidou, _beidou);
  SETTER_BOOL(Config_constellation_type, Beidou, _beidou);

  GETTER_SETTER(UpdateType, Config_constellation_type, update_type, _update_type);


  Q_constellation_type::Q_constellation_type(void) :
    Input_message_with_subid(0x64, 0x1A)
  {}

  RESPONSE2(Q_constellation_type, 0x64, 0x8C);


  Config_leap_seconds::Config_leap_seconds(int8_t s, UpdateType ut) :
    Input_message_with_subid(0x64, 0x1f),
    _seconds(s), _update_type(ut)
  {}

  GETTER(Payload_length, Config_leap_seconds, body_length, 2);

  void Config_leap_seconds::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _seconds);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(int8_t, Config_leap_seconds, seconds, _seconds);
  GETTER_SETTER(UpdateType, Config_leap_seconds, update_type, _update_type);

  Q_GPS_time::Q_GPS_time(void) :
    Input_message_with_subid(0x64, 0x20)
  {}

  RESPONSE2(Q_GPS_time, 0x64, 0x8E);


  Config_GNSS_datum_index::Config_GNSS_datum_index(uint16_t i, UpdateType ut) :
    Input_message_with_subid(0x64, 0x27),
    _datum_index(i),
    _update_type(ut)
  {}

  GETTER(Payload_length, Config_GNSS_datum_index, body_length, 5);

  void Config_GNSS_datum_index::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _datum_index);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(int16_t, Config_GNSS_datum_index, datum_index, _datum_index);
  GETTER_SETTER(UpdateType, Config_GNSS_datum_index, update_type, _update_type);

  Q_GNSS_datum_index::Q_GNSS_datum_index(void) :
    Input_message_with_subid(0x64, 0x28)
  {}

  RESPONSE2(Q_GNSS_datum_index, 0x64, 0x92);


  Config_1PPS_pulse_width::Config_1PPS_pulse_width(uint32_t w, UpdateType ut) :
    Input_message_with_subid(0x65, 0x01),
    _width(w), _update_type(ut)
  {}

  Config_1PPS_pulse_width::Config_1PPS_pulse_width(double w, UpdateType ut) :
    Input_message_with_subid(0x65, 0x01),
    _width(w * 1e+6), _update_type(ut)
  {}

  GETTER(Payload_length, Config_1PPS_pulse_width, body_length, 5);

  void Config_1PPS_pulse_width::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _width);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER_MOD(double, Config_1PPS_pulse_width, width, _width, _width * 1.0e-6, val * 1e+6);
  GETTER_SETTER_RAW(int32_t, Config_1PPS_pulse_width, width, _width);
  GETTER_SETTER(UpdateType, Config_1PPS_pulse_width, update_type, _update_type);


  Q_1PPS_pulse_width::Q_1PPS_pulse_width(void) :
    Input_message_with_subid(0x65, 0x02)
  {}

  RESPONSE2(Q_1PPS_pulse_width, 0x65, 0x80);


  Config_1PPS_freq_output::Config_1PPS_freq_output(uint32_t freq, UpdateType ut) :
    Input_message_with_subid(0x65, 0x03),
    _frequency(freq), _update_type(ut)
  {}

  GETTER(Payload_length, Config_1PPS_freq_output, body_length, 5);

  void Config_1PPS_freq_output::body_to_buf(std::vector<uint8_t>& buffer) const {
    append_be(buffer, _frequency);
    append_be(buffer, static_cast<uint8_t>(_update_type));
  }

  GETTER_SETTER(uint32_t, Config_1PPS_freq_output, frequency, _frequency);
  GETTER_SETTER(UpdateType, Config_1PPS_freq_output, update_type, _update_type);


  Q_1PPS_freq_output::Q_1PPS_freq_output(void) :
    Input_message_with_subid(0x65, 0x04)
  {}


}; // namespace SkyTraqBin

#undef GETTER
#undef GETTER_SETTER
#undef SETTER_BOOL
#undef GETTER_RAW
#undef GETTER_SETTER_RAW
#undef GETTER_MOD
#undef GETTER_SETTER_MOD
#undef GETTER_ITERATORS
#undef RESPONSE1
#undef RESPONSE2
