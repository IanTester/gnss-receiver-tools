/*
        Copyright 2016 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <map>
#include "UBX.hh"
#include "LE.hh"

/*
  Sources:
*/

namespace UBX {

  namespace Cfg {

    void Q_prt::body_to_buf(std::vector<uint8_t>& buffer) const {
      if (!_current_port)
	append_le(buffer, static_cast<uint8_t>(_portid));
    }

    Prt::Prt(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
      Input_Output_message(Class_ID::CFG, 0x00)
    {
      _portid = static_cast<PortID>(extract_le<uint8_t>(payload));
      if (payload <= payload_last) {
	uint16_t txReady = extract_le<uint16_t>(payload);
	_txr_en = txReady & 0x0001;
	if (_txr_en) {
	  _txr_pol = txReady & 0x0002;
	  _txr_pin = (txReady >> 2) & 0x1f;
	  _txr_thres = txReady >> 7;
	}

	uint32_t mode = extract_le<uint32_t>(payload);
	_mode_charlen = static_cast<Mode_Charlen>((mode >> 6) & 0x03);
	_mode_parity = static_cast<Mode_Parity>((mode >> 9) & 0x07);
	_mode_stopbits = static_cast<Mode_Stopbits>((mode >> 12) & 0x03);

	_baudrate = extract_le<uint32_t>(payload);

	uint16_t inproto = extract_le<uint16_t>(payload);
	_inproto_ubx = inproto & 0x0001;
	_inproto_nmea = (inproto >> 1) & 0x0001;
	_inproto_rtcm = (inproto >> 2) & 0x0001;

	uint16_t outproto = extract_le<uint16_t>(payload);
	_outproto_ubx = outproto & 0x0001;
	_outproto_nmea = (outproto >> 1) & 0x0001;

	uint16_t flags = extract_le<uint16_t>(payload);
	_flags_exttxtimeout = (flags >> 1) & 0x0001;
      }
    }

    void Prt::body_to_buf(std::vector<uint8_t>& buffer) const {
      append_le(buffer, static_cast<uint8_t>(_portid));
      append_le<uint8_t>(buffer, 0);	// reserved

      uint16_t txReady = 0;
      if (_txr_en) {
	txReady = 1;
	txReady |= static_cast<uint16_t>(_txr_pol) << 1;
	txReady |= (static_cast<uint16_t>(_txr_pin) & 0x1f) << 2;
	txReady |= (_txr_thres & 0x1ff) << 7;
      }
      append_le(buffer, txReady);

      uint32_t mode = 0;
      mode |= (static_cast<uint32_t>(_mode_charlen) & 0x03) << 6;
      mode |= (static_cast<uint32_t>(_mode_parity) & 0x07) << 9;
      mode |= (static_cast<uint32_t>(_mode_stopbits) & 0x03) << 12;
      append_le(buffer, mode);

      append_le(buffer, _baudrate);
      append_le<uint16_t>(buffer, _inproto_ubx
			  | (static_cast<uint16_t>(_inproto_nmea) << 1)
			  | (static_cast<uint16_t>(_inproto_rtcm) << 2));
      append_le<uint16_t>(buffer, _outproto_ubx
			  | (static_cast<uint16_t>(_outproto_nmea) << 1));
      append_le<uint16_t>(buffer, static_cast<uint16_t>(_flags_exttxtimeout) << 1);
      append_le<uint8_t>(buffer, 0);	// reserved[2]
      append_le<uint8_t>(buffer, 0);
    }

    void Q_msg::body_to_buf(std::vector<uint8_t>& buffer) const {
      append_le(buffer, _msg_class);
      append_le(buffer, _msg_id);
    }

    Msg::Msg(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
      Input_Output_message(Class_ID::CFG, 0x01)
    {
      _msg_class = extract_le<uint8_t>(payload);
      _msg_id = extract_le<uint8_t>(payload);
      if (payload == payload_last) {
	_rates[0] = extract_le<uint8_t>(payload);
	_current_port = true;
      } else {
	for (uint8_t i = 0; i < 6; i++) {
	  _rates[i] = extract_le<uint8_t>(payload);
	  _current_port = true;
	}
      }
    }

    std::string Msg::message_name(void) const {
      std::map<uint8_t, std::map<uint8_t, std::string> > names { { 0xf0, { { 0x00, "GGA" },
								           { 0x01, "GLL" },
									   { 0x02, "GSA" },
									   { 0x03, "GSV" },
									   { 0x04, "RMC" },
									   { 0x05, "VTG" },
									   { 0x06, "GRS" },
									   { 0x07, "GST" },
									   { 0x08, "ZDA" },
									   { 0x09, "GBS" },
									   { 0x0a, "DTM" },
									   { 0x0d, "GNS" },
									   { 0x0f, "VLW" },
									   { 0x40, "GPQ" },
									   { 0x41, "TXT" },
									   { 0x42, "GNQ" },
									   { 0x43, "GLQ" },
									   { 0x44, "GBQ" } } },
	                                                         { 0xf1, { { 0x00, "POSITION" },
								           { 0x03, "SVSTATUS" },
									   { 0x04, "TIME" },
									   { 0x40, "RATE" },
									   { 0x41, "CONFIG" } } } };
      auto cls = names.find(_msg_class);
      if (cls != names.end()) {
	auto name = cls->second.find(_msg_id);
	if (name != cls->second.end())
	  return name->second;
      }
      return "";
    }

    void Msg::body_to_buf(std::vector<uint8_t>& buffer) const {
      append_le(buffer, _msg_class);
      append_le(buffer, _msg_id);
      if (_current_port)
	append_le(buffer, _rates[0]);
      else
	for (uint8_t i = 0; i < 6; i++)
	  append_le(buffer, _rates[i]);
    }

    GNSS::GNSS(std::vector<uint8_t>::const_iterator& payload, const std::vector<uint8_t>::const_iterator& payload_last) :
      Input_Output_message(Class_ID::CFG, 0x3e)
    {
      _msg_ver = extract_le<uint8_t>(payload);
      _tracking_channels_hw = extract_le<uint8_t>(payload);
      _tracking_channels_use = extract_le<uint8_t>(payload);

      uint8_t numconfigs = extract_le<uint8_t>(payload);
      for (uint8_t i = 0; i < numconfigs; i++) {
	auto id = extract_le<uint8_t>(payload);
	auto mintc = extract_le<uint8_t>(payload);
	auto maxtc = extract_le<uint8_t>(payload);
	payload++; // ?
	auto flags = extract_le<uint32_t>(payload);
	_configs.emplace_back(static_cast<GNSS_ID>(id), mintc, maxtc,
			      flags & 0x01, flags >> 16);
      }
    }

    void GNSS::body_to_buf(std::vector<uint8_t>& buffer) const {
      append_le(buffer, _msg_ver);
      append_le(buffer, _tracking_channels_hw);
      append_le(buffer, _tracking_channels_use);
      append_le<uint8_t>(buffer, _configs.size());

      for (auto config : _configs) {
	append_le(buffer, static_cast<uint8_t>(config.gnss_id));
	append_le(buffer, config.min_tracking_channels);
	append_le(buffer, config.max_tracking_channels);
	append_le<uint8_t>(buffer, 0);

	uint32_t flags = config.enabled
	  | (static_cast<uint32_t>(config.signal_config_mask) << 16);
	append_le(buffer, flags);
      }
    }

  }; // namespace Cfg

}; // namespace UBX
