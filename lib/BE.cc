/*
        Copyright 2014 Ian Tester

        This file is part of GNSS receiver tools.

        GNSS receiver tools is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        GNSS receiver tools is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with GNSS receiver tools.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "BE.hh"

// Write a boolean as a single byte
template <>
void append_be<bool>(std::vector<uint8_t>& buffer, bool val) {
  buffer.push_back(static_cast<uint8_t>(val));
}

template <>
void append_be<int8_t>(std::vector<uint8_t>& buffer, int8_t val) {
  buffer.push_back(static_cast<uint8_t>(val));
}

template <>
void append_be<uint8_t>(std::vector<uint8_t>& buffer, uint8_t val) {
  buffer.push_back(val);
}

template <>
void append_be<int16_t>(std::vector<uint8_t>& buffer, int16_t val) {
  buffer.reserve(buffer.size() + 2);

  buffer.push_back(val >> 8);
  buffer.push_back(val & 0xff);
}

template <>
void append_be<uint16_t>(std::vector<uint8_t>& buffer, uint16_t val) {
  buffer.reserve(buffer.size() + 2);

  buffer.push_back(val >> 8);
  buffer.push_back(val & 0xff);
}

template <>
void append_be<int32_t>(std::vector<uint8_t>& buffer, int32_t val) {
  buffer.reserve(buffer.size() + 4);

  buffer.push_back(val >> 24);
  buffer.push_back((val >> 16) & 0xff);
  buffer.push_back((val >> 8) & 0xff);
  buffer.push_back(val & 0xff);
}

template <>
void append_be<uint32_t>(std::vector<uint8_t>& buffer, uint32_t val) {
  buffer.reserve(buffer.size() + 4);

  buffer.push_back(val >> 24);
  buffer.push_back((val >> 16) & 0xff);
  buffer.push_back((val >> 8) & 0xff);
  buffer.push_back(val & 0xff);
}

template <>
void append_be<float>(std::vector<uint8_t>& buffer, float val) {
  buffer.reserve(buffer.size() + 4);

  auto *mem = reinterpret_cast<uint8_t*>(&val);
#if __BYTE_ORDER == __LITTLE_ENDIAN
  buffer.push_back(mem[3]);
  buffer.push_back(mem[2]);
  buffer.push_back(mem[1]);
  buffer.push_back(mem[0]);
#elif __BYTE_ORDER == __BIG_ENDIAN
  buffer.push_back(mem[0]);
  buffer.push_back(mem[1]);
  buffer.push_back(mem[2]);
  buffer.push_back(mem[3]);
#endif
}

template <>
void append_be<double>(std::vector<uint8_t>& buffer, double val) {
  buffer.reserve(buffer.size() + 8);

  auto *mem = reinterpret_cast<uint8_t*>(&val);
#if __BYTE_ORDER == __LITTLE_ENDIAN
  buffer.push_back(mem[7]);
  buffer.push_back(mem[6]);
  buffer.push_back(mem[5]);
  buffer.push_back(mem[4]);
  buffer.push_back(mem[3]);
  buffer.push_back(mem[2]);
  buffer.push_back(mem[1]);
  buffer.push_back(mem[0]);
#elif __BYTE_ORDER == __BIG_ENDIAN
  buffer.push_back(mem[0]);
  buffer.push_back(mem[1]);
  buffer.push_back(mem[2]);
  buffer.push_back(mem[3]);
  buffer.push_back(mem[4]);
  buffer.push_back(mem[5]);
  buffer.push_back(mem[6]);
  buffer.push_back(mem[7]);
#endif
}

template <>
bool extract_be<bool>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = static_cast<bool>(buffer[0]);
  buffer++;
  return val;
}

template <>
int8_t extract_be<int8_t>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = static_cast<int8_t>(buffer[0]);
  buffer++;
  return val;
}

template <>
uint8_t extract_be<uint8_t>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = buffer[0];
  buffer++;
  return val;
}

template <>
int16_t extract_be<int16_t>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = (static_cast<int16_t>(buffer[0]) << 8) | buffer[1];
  buffer += 2;
  return val;
}

template <>
uint16_t extract_be<uint16_t>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = (static_cast<uint16_t>(buffer[0]) << 8) | buffer[1];
  buffer += 2;
  return val;
}

template <>
int32_t extract_be<int>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = (static_cast<int32_t>(buffer[0]) << 24)
    | (static_cast<int32_t>(buffer[1]) << 16)
    | (static_cast<int32_t>(buffer[2]) << 8)
    | static_cast<int32_t>(buffer[3]);
  buffer += 4;
  return val;
}

template <>
uint32_t extract_be<uint32_t>(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = (static_cast<uint32_t>(buffer[0]) << 24)
    | (static_cast<uint32_t>(buffer[1]) << 16)
    | (static_cast<uint32_t>(buffer[2]) << 8)
    | static_cast<uint32_t>(buffer[3]);
  buffer += 4;
  return val;
}

uint32_t extract_be24(std::vector<uint8_t>::const_iterator& buffer) {
  auto val = (static_cast<uint32_t>(buffer[0]) << 16)
    | (static_cast<uint32_t>(buffer[1]) << 8)
    | static_cast<uint32_t>(buffer[2]);
  buffer += 3;
  return val;
}

template <>
float extract_be<float>(std::vector<uint8_t>::const_iterator& buffer) {
  float val;
  auto *mem = reinterpret_cast<uint8_t*>(&val);
#if __BYTE_ORDER == __LITTLE_ENDIAN
  mem[3] = buffer[0];
  mem[2] = buffer[1];
  mem[1] = buffer[2];
  mem[0] = buffer[3];
#elif __BYTE_ORDER == __BIG_ENDIAN
  mem[0] = buffer[0];
  mem[1] = buffer[1];
  mem[2] = buffer[2];
  mem[3] = buffer[3];
#endif
  buffer += 4;
  return val;
}

template <>
double extract_be<double>(std::vector<uint8_t>::const_iterator& buffer) {
  double val;
  auto *mem = reinterpret_cast<uint8_t*>(&val);
#if __BYTE_ORDER == __LITTLE_ENDIAN
  mem[7] = buffer[0];
  mem[6] = buffer[1];
  mem[5] = buffer[2];
  mem[4] = buffer[3];
  mem[3] = buffer[4];
  mem[2] = buffer[5];
  mem[1] = buffer[6];
  mem[0] = buffer[7];
#elif __BYTE_ORDER == __BIG_ENDIAN
  mem[0] = buffer[0];
  mem[1] = buffer[1];
  mem[2] = buffer[2];
  mem[3] = buffer[3];
  mem[4] = buffer[4];
  mem[5] = buffer[5];
  mem[6] = buffer[6];
  mem[7] = buffer[7];
#endif
  buffer += 8;
  return val;
}


